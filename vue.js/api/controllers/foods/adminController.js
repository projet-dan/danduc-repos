"use strict";

const { OK, BAB_REQUEST } = require('../../config');
const passwordHash = require("password-hash");

module.exports = function(app, food) {
  app.get("/admin/:username/:password", function(req, res, next) {
      food.query("SELECT password FROM admins WHERE username = ? AND active = ?",
                 [req.params.username, 1],
                 function(error, result) {
					 console.log('tess');
					 console.log(result);
					  let isFound = false;
                      if (error)
                      {
                         console.error(`${error.code}\n${error.sqlMessage}`);
                         return res.status(BAB_REQUEST).send();
                      }
					  if(result.length > 0)
						  isFound = passwordHash.verify(req.params.password, result[0].password);  
                      return res.status(OK).send({
						success: isFound,
                      });
					  next();
                 });
      });
};
