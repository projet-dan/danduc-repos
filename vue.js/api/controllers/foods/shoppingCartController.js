"use strict";

const { OK, BAB_REQUEST } = require('../../config');

module.exports = function(app, food) {
  app.get("/shoppingCartFood/:categorieId", function(req, res, next) {
    const categoryId = req.params.categorieId;
    if (!categoryId || categoryId === undefined)
      return res.status(BAB_REQUEST).send();
    food.query("SELECT * FROM produits WHERE categorie_id = ? AND discontinue = ? AND active = ? ORDER BY nom",
      [categoryId, 0, 1],
      function(error, results) {
        if (error) 
        {
           console.error(`${error.code}\n${error.sqlMessage}`);
           return res.status(BAB_REQUEST).send();
        }
        return res.status(OK).send(results);
      });
  });

  app.get("/shoppingCartFood", function(req, res, next) {
    food.query("SELECT * FROM produits WHERE discontinue = ? AND active = ? ORDER BY nom",
      [0, 1],
      function(error, results) {
        if (error)
        {
            console.error(`${error.code}\n${error.sqlMessage}`);
            return res.status(BAB_REQUEST).send();
        }
        return res.status(OK).send(results);
      });
  });
};
