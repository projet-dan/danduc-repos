"use strict";

const { OK, BAB_REQUEST } = require('../../config');
const passwordHash = require("password-hash");
const moment = require("moment");

module.exports = function(app, food) {
  app.post("/employee", function(req, res, next) {
    const { 
            nom,
            prenom,
            abrege,
            titre,
            statut,
            genre,
            adresse,
            ville,
            region,
            code_postale,
            pays,
            telephone,
            cellulaire,
            courriel,
            date_naissance,
            date_embauche,
            salaire,
            par,
            conge_vacance,
            conge_maladie,
            photo,
            note } = req.body.employee;
    const { username,password } = req.body.admin;
    food.query(
      "INSERT employees(nom," +
        "prenom," +
        "abrege," +
        "titre," +
        "statut," +
        "genre," +
        "adresse," +
        "ville," +
        "region," +
        "code_postale," +
        "pays," +
        "telephone," +
        "cellulaire," +
        "courriel," +
        "date_naissance," +
        "date_embauche," +
        "salaire," +
        "par," +
        "conge_vacance," +
        "conge_maladie," +
        "photo," +
        "note," +
        "created_at) " +
        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        nom,
        prenom,
        abrege,
        titre,
        statut,
        genre,
        adresse,
        ville,
        region,
        code_postale,
        pays,
        telephone,
        cellulaire,
        courriel,
        date_naissance,
        date_embauche,
        salaire,
        par,
        conge_vacance,
        conge_maladie,
        photo,
        note,
        moment().format("YYYY-MM-DD hh:mm:ss")
      ],
      function(error, result) {
        if (error || !result || result.insertId === 0)
        {
            if(error)
              console.error(`${error.code}\n${error.sqlMessage}`);
            else
              console.error(`row(s) inserted : ${result.insertId}`);
            return res.status(BAB_REQUEST).send();
        }
        password = passwordHash.generate(password);
        food.query("INSERT admins (employee_id, username, password, confirm_password, created_at) VALUES(?,?,?,?,?)",
                    [
                      result.insertId,
                      username,
                      password,
                      password,
                      moment().format("YYYY-MM-DD hh:mm:ss")
                    ],
                    function(err, response) {
                        if (err || !response || response.insertId === 0)
                        {
                            if(err)
                              console.error(`${err.code}\n${err.sqlMessage}`);
                            else
                              console.error(`row(s) inserted : ${response.insertId}`);
                            return res.status(BAB_REQUEST).send();
                        }
                        return res.status(OK).send({ success: true });
                    }); 
      });
  });
};
