"use strict";

const { OK, BAB_REQUEST } = require('../../config');
const multer = require("multer");
const moment = require("moment");
multer({ limits: { fileSize: 100000000 } });

module.exports = function(app, food) 
{
  app.get("/categorie", function(req, res, next) 
  {
    food.query("SELECT * FROM categories ORDER BY nom", function(error,results) 
    {
        if(error)
        {
            console.error(`${error.code}\n${error.sqlMessage}`);
            return res.status(BAB_REQUEST).send();
        }
        return res.status(OK)
                  .send(results);         
    }); 
  });

  app.get("/categorie/:id", function(req, res, next) 
  {
    const categoryId = req.params.id;
    if (!categoryId || categoryId === undefined) 
       return res.status(BAB_REQUEST).send();
    food.query("SELECT * FROM categories WHERE id = ?", categoryId, function(error,result) 
    {
      if(error) 
      {
         console.error(`${error.code}\n${error.sqlMessage}`);
         return res.status(BAB_REQUEST).send();
      }
      return res.status(OK)
                .send(result[0]);
    });
  });

  app.post("/categorie", function(req, res, next) 
  {
    const { nom,description,photo } = req.body;
    food.query("INSERT categories (nom,description,photo,created_at) VALUES(?,?,?,?)",
        [nom,description,photo,moment().format("YYYY-MM-DD hh:mm:ss")],
        function(error, result) 
        {
          if (error || !result || result.insertId === 0)
          {
              if(error)
                console.error(`${error.code}\n${error.sqlMessage}`);
              else
                console.error(`row(s) inserted : ${result.insertId}`);
              return res.status(BAB_REQUEST).send();
          }
          return res.status(OK).send({ success: true });
        }
    );
  });

  app.put("/categorie", function(req, res, next) 
  {
    const { id,nom,description,photo,active } = req.body;
    food.query("UPDATE categories SET nom = ?, description=?, photo = ?, active = ?, updated_at = ? WHERE id = ?",
      [nom,description,photo,active,moment().format("YYYY-MM-DD hh:mm:ss"),id],function(error, result) 
      {
        if (error || !result || result.affectedRows === 0) 
        {
            if(error)
              console.error(`${error.code}\n${error.sqlMessage}`);
            else
               console.error(`row(s) updated : ${result.affectedRows}`);
            return res.status(BAB_REQUEST).send();
        }
        food.query("SELECT count(*) AS num FROM produits WHERE categorie_id = ?",[id],function(error, result) 
        {
            if (error || result.length === 0 && result[0].num === 0) 
            {
                if(error)
                  console.error(`${error.code}\n${error.sqlMessage}`);
                else
                  console.error(`row(s) found : ${result[0].num}`);
                return res.status(BAB_REQUEST).send();
            }
            food.query("UPDATE produits SET active = ? WHERE categorie_id = ?",[active, id],function(error, result) 
            {
                if (error || !result || result.affectedRows === 0)
                {
                    if(error)
                       console.error(`${error.code}\n${error.sqlMessage}`);
                    else 
                       console.error(`row(s) updated : ${result.affectedRows}`);
                    return res.status(BAB_REQUEST).send();
                }
            });
        });
        return res.status(OK).send({ success: true });
      }
    );
  });

  app.delete("/categorie/:id", function(req, res, next) {
    let categoryId = req.params.id;
    if (!categoryId || categoryId === undefined) 
       res.status(BAB_REQUEST).send();
    food.query("UPDATE categories SET active = ? WHERE id = ?",[0, categoryId],function(error, result) 
    {
        if (error || !result || result.affectedRows === 0)
        { 
            if(error)
              console.error(`${error.code}\n${error.sqlMessage}`);
            else
              console.error(`row(s) updated : ${result.affectedRows}`);
           return res.status(BAB_REQUEST).send();
        }
        food.query("SELECT count(*) AS num FROM produits WHERE categorie_id = ? AND active = ?",[categoryId, 1],function(err, response) {
            if (err || response.length === 0 || response[0].num === 0)
            {
               if(err)
                  console.error(`${err.code}\n${err.sqlMessage}`);
                else
                  console.error(`row(s) found ${response[0].num}`);
               return res.status(BAB_REQUEST).send();
            }
            food.query("UPDATE produits SET active = ? WHERE categorie_id = ?",[0, categoryId],function(err2, response2) {
                if (err2 || !response2 || response2.affectedRows === 0) 
                {
                    if(err2)
                      console.error(`${err2.code}\n${err2.sqlMessage}`);
                    else
                      console.error(`row(s) updated : ${response2.affectedRows}`);
                   return res.status(BAB_REQUEST).send();
                }
            });
        });
        return res.status(OK).send({ success: true });
      }
    );
  });
};
