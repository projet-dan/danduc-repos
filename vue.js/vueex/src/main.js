import Vue from "vue";
import App from "./App";
import router from "./router";
import { store } from './store/store';

import "./assets/css/vendor/fontawesome/css/font-awesome.css";
import "./assets/css/vendor/metisMenu/dist/metisMenu.css";
import "./assets/css/vendor/animate.css/animate.css";
import "./assets/css/vendor/bootstrap/dist/css/bootstrap.css";
import "./assets/css/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css";
import "./assets/css/fonts/pe-icon-7-stroke/css/helper.css";
import "./assets/css/styles/style.css";

//import * as VueGoogleMaps from "vue2-google-maps";
import VueCurrencyFilter from "vue-currency-filter";
import Vuelidate from "vuelidate";

Vue.config.productionTip = false;

Vue.use(VueCurrencyFilter, {
  symbol: "$",
  thousandsSeparator: ".",
  fractionCount: 2,
  fractionSeparator: ".",
  symbolPosition: "front",
  symbolSpacing: false
});

Vue.use(Vuelidate);

export const bus = new Vue();

new Vue({ 
  el: '#app', 
  router, 
  store,
  render: h => h(App) })

import "@/assets/js/vendor/jquery/dist/jquery.min.js";
import "@/assets/js/vendor/jquery-ui/jquery-ui.min.js";
import "@/assets/js/vendor/slimScroll/jquery.slimscroll.min.js";
import "@/assets/js/vendor/bootstrap/dist/js/bootstrap.min.js";
import "@/assets/js/vendor/metisMenu/dist/metisMenu.min.js";
import "@/assets/js/vendor/sparkline/index.js";
