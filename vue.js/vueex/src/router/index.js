/*
* Program : Routing
* Écrit par : Dan Duc Dao
*/

import Vue from "vue";
import Router from "vue-router";
import Admin from "@/components/admin/index";
import ListeProduit from "@/components/admin/product/index";
import ProductDetail from "@/components/admin/product/ProductDetail";
import ListeCategorie from "@/components/admin/category/index";
import CategoryDetail from "@/components/admin/category/CategoryDetail";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Catgory",
      component: ListeCategorie
    },
    {
      path: "/categorie/:id",
      name: "UpdateCategorie",
      component: CategoryDetail
    },
    {
      path: "/categorie/new",
      name: "NewCategorie",
      component: CategoryDetail
    },
    {
      path: "/produit",
      name: "ListeProduit",
      component: ListeProduit,
    },
    {
      path: "/produit/new",
      name: "UpdateProduit",
      component: ProductDetail
    },
    {
      path: "/produit/:id",
      name: "UpdateProduit",
      component: ProductDetail
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
