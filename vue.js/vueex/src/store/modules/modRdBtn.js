const state = {
    STR_CHECKED : " checked",
    selActiveOptions : [],
    selDiscontinueOptions : [],
};

const checkedByValue = (firstRdBtn, secondRdBtn, value) => {
    return (value === 1)?firstRdBtn.clsAttribut += state.STR_CHECKED:secondRdBtn.clsAttribut += state.STR_CHECKED;
};

const checkedById = (firstRdBtn, secondRdBtn, id) => {
    if(firstRdBtn.id === id)
    {
        firstRdBtn.clsAttribut += state.STR_CHECKED;
        secondRdBtn.clsAttribut = secondRdBtn.clsAttribut.split(" ")[0];
    }else{
        secondRdBtn.clsAttribut += state.STR_CHECKED;
        firstRdBtn.clsAttribut = firstRdBtn.clsAttribut.split(" ")[0];   
    }
};

const getters = {
    selActiveOptions : (state) => {
       return state.selActiveOptions;
    },
    selDiscontinueOptions : (state) => {
        return state.selDiscontinueOptions;
    },
    str_checked : (state) => {
        return state.STR_CHECKED;
    }
};

const mutations =  {
    activeOptions : (state,options) => {
       state.selActiveOptions = options;
    },
    firstActiveChecked : (state, selectedValue) => {
        let [isActive, isNotActive]= state.selActiveOptions;
        checkedByValue(isActive, isNotActive,selectedValue);
        state.selActiveOptions = [isActive, isNotActive];
    },
    selectedActiveItem : (state,id) => {
        let [isActive, isNotActive]= state.selActiveOptions;
        checkedById(isActive, isNotActive,id);
        state.selActiveOptions = [isActive, isNotActive];
    },
    discontinueOptions : (state,options) => {
       state.selDiscontinueOptions = options;
    },
    firstContinueChecked : (state,selectedValue) => {
        let [isDiscontinue, isNotDiscontinue]= state.selDiscontinueOptions;
        checkedByValue(isDiscontinue, isNotDiscontinue,selectedValue);
        state.selDiscontinueOptions = [isDiscontinue, isNotDiscontinue];
    },
    selectedDiscontinueItem : (state,id) => {
        let [isDiscontinue, isNotDiscontinue]= state.selDiscontinueOptions;
        checkedById(isDiscontinue, isNotDiscontinue,id);
        state.selDiscontinueOptions = [isDiscontinue, isNotDiscontinue];
    }  
};

const actions = {
    firstActiveChecked : ({ commit },selectedValue) => {
        commit('firstActiveChecked',selectedValue);
    },
    selectedActiveItem : ({ commit },id) => {
        commit('selectedActiveItem',id);
    },
    firstContinueChecked : ({ commit },selectedValue) => {
        commit('firstContinueChecked',selectedValue);
    },
    selectedDiscontinueItem : ({ commit },id) => {
        commit('selectedDiscontinueItem',id);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}


