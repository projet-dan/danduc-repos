const state = {
    selOptions : [
        { id:1, clsAttribut:'icheckbox_square-green', checked:false, name:'name',text:'Name',value:'nom' },
        { id:2, clsAttribut:'icheckbox_square-green', checked:false, name:'category',text:'Category',value:'categorie_nom' },
        { id:3, clsAttribut:'icheckbox_square-green', checked:false, name:'quantity',text:'Quantity',value:'quantite_en_stock' },
        { id:4, clsAttribut:'icheckbox_square-green', checked:false, name:'price',text:'Price',value:'prix' },
    ],
    term : "",
    currentItemsFound:[]
};

const getters = {
    options : (state) => {
        return state.selOptions;
    },
    currentItemsFound: (state) => {
        return state.currentItemsFound;
    },
    term : (state) => {
        return state.term;
    }
};

const mutations = {
    selectedItem : (state,id) => {
        let currentItem = [...state.selOptions];
        currentItem = currentItem.reduce((newItem,oldItem) => {  
            if(oldItem.id === id)
            {
                oldItem.checked = !oldItem.checked;
                oldItem.clsAttribut = oldItem.checked === true? oldItem.clsAttribut + " checked":oldItem.clsAttribut.split(" ")[0];
            }
            newItem.push(oldItem)
            return newItem;
        },[]);
        state.selOptions = [...currentItem];
    },
    doSearch : (state,payload) => {
         let { term,currentItems } = { ...payload };
         let results = [];
         if(term)
         {
            state.selOptions.forEach(element => {
                if(element.checked === true)
                { 
                    results = results.concat(currentItems.filter(item => item[element.value] != null && item[element.value].toLowerCase().indexOf(term.toLowerCase()) >= 0));
                }
            });
            state.term = term;
         }
         state.currentItemsFound = results;
    }
};

const actions = {
    selectedItem : ({ commit }, payload) => {
        commit('selectedItem',payload)
    },
    doSearch : ({ commit },payload) => {
        commit('doSearch',payload);
    }
};

export default {
     state,
     getters,
     mutations,
     actions
};
