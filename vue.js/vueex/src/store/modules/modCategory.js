import { srvCategory } from "../../services/srvCategory";
import clsCategory from "../../classes/clsCategory";
const srvCategoryObj = new srvCategory();

const state = {
    categories: [],
    clsCategoryObj : new clsCategory(),
    flag : 0
}

const getters = {
    categories : (state) => {
        return state.categories;
    },
    category : (state) => {
        return state.clsCategoryObj;
    },
    flag : (state) => {
        return state.flag;
    }
}

const mutations = {
    categories : (state, items) => {
        if(Object.keys(items).length > 0)
            state.categories = items;
    },
    setCategoryListEmpty : (state) => {
        state.clsCategoryObj = {};
    },
    flag : (state,num) => {
        state.flag = num
    }
};

const actions = {
    loadCategories : ({ commit }) => {  
        srvCategoryObj.categorieListe()
                      .then(res => commit('categories', res.body),
                            err => console.log(err));
    },
    loadCategoryById : ({ dispatch },categorieId) =>{
        srvCategoryObj.getCategorieById(categorieId).then(
            res => {
                let categorie = res.body;
                if (Object.keys(categorie).length > 0) {
                    let { clsCategoryObj } = state;
                    clsCategoryObj.id = categorie.id;
                    clsCategoryObj.nom = categorie.nom;
                    clsCategoryObj.description = categorie.description;
                    clsCategoryObj.photo = categorie.photo;
                    clsCategoryObj.active = categorie.active;
                    dispatch('firstActiveChecked',clsCategoryObj.active);
                }
            },
            err => console.log(err)
        );
    },
    saveCatgoryById : ({ commit },categoryObj,myObject) => {
        srvCategoryObj.updateCategorie(categoryObj)
                      .then(res => res.body.success
                                   ? commit('flag',1)
                                   : alert("Item a été sauvegardé avec sans succès"),
                            err => console.log(err)
                        );
    },
    saveCatgory : ({ commit },categoryObj,myObject) => {
       
        srvCategoryObj.saveCategorie(categoryObj)
                             .then(res =>res.body.success
                                         ? commit('flag',1)
                                         : alert("Item a été sauvegardé avec sans succès"),
                                   err => console.log(err)
                                );
    },
    removeCategories : ({ dispatch },categoryId) => {
        if (confirm("Êtes-vous sûre de vouloir supprimer cet item?")) {
            srvCategoryObj.removeCategorieById(categoryId).then(
              res => {
                if (res.body.success) {
                  alert("Catégorie a été supprimée avec succès");
                  dispatch('loadCategories');
                } else {
                  alert("Catégorie a été supprimée avec sans succès");
                }
              },
              err => console.log(err)
            );
          }
    },
}

export default {
    state,
    getters,
    mutations,
    actions
};
