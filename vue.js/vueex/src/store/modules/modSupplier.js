import { srvSupplier } from "../../services/srvSupplier";
const srvSupplierObj = new srvSupplier();

const state = {
    suppliers: []
};

const getters = {
    suppliers : (state) => {
        return state.suppliers;
    }
}
const mutations = {
    suppliers : (state,items) => {
        if(Object.keys(items).length > 0)
            state.suppliers = items;
    }
};

const actions = { 
    loadSuppliers : ({ commit }) => {
        srvSupplierObj.fournisseurListe()
                      .then(res =>commit('suppliers', res.body),
                            err => console.log(err)); 
    }
}

export default {
    state,
    getters,
    mutations,
    actions
};





