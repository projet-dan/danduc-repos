import { srvProduct } from "../../services/srvProduct";
import { clsProduct } from "../../classes/clsProduct";

const srvProductObj = new srvProduct();

const state = {
    currentItems : [],
    clsProductObj: new clsProduct() 
};

const getters = {
    currentItems : (state) => {
        return state.currentItems;
    },
    product : (state) => {
        return state.clsProductObj;
    }

};

const mutations = {
    load : (state,payload) => {
        state.currentItems = payload;
    },
    setProduct : (state,item) => {
        if(Object.keys(item).length > 0)
        {
            state.clsProductObj.id = item.id;
            state.clsProductObj.nom = item.nom;
            state.clsProductObj.categorie_id = item.categorie_id;
            state.clsProductObj.fournisseur_id = item.fournisseur_id;
            state.clsProductObj.quantite_par_unite = item.quantite_par_unite;
            state.clsProductObj.prix = item.prix;
            state.clsProductObj.quantite_en_stock = item.quantite_en_stock;
            state.clsProductObj.quantite_commande = item.quantite_commande;
            state.clsProductObj.reapprovisionnement = item.reapprovisionnement;
            state.clsProductObj.discontinue = item.discontinue;
            state.clsProductObj.active = item.active;
        }
    },
    setProductListEmpty : (state) => {
        state.clsProductObj = {};
    },
    message : (state,str) => {
        alert(`Item a été sauvegardé ${ str }`);
    }
};

const actions = {
    load : ({ commit }) => {
        srvProductObj.produitListe().then(
            res => {
              if (Object.keys(res.body).length > 0) {
                commit('load', res.body);
              }
            },
            err => {
              console.log(err);
            }
          );   
    },
    loadProductById : ({ commit, dispatch }, productId) => {
        srvProductObj.getProduitById(productId)
                      .then( 
                              res => {
                                   let product = res.body;
                                   commit('setProduct',product);
                                   dispatch('firstActiveChecked',product.active);
                                   dispatch('firstContinueChecked',product.discontinue);
                                },
                              err => console.log(err)
                        );
                       
    },
    remove : ({ commit , dispatch },payload) => {
        let { productId, term } = { ...payload };
        if (confirm("Êtes-vous sûre de vouloir supprimer cet item?")) {
            produitService.removeProduitById(productId).then(
                res => {
                  if (res.body.success) 
                  {
                    //Reload data from bd
                    produitService.produitListe().then(
                        res => {
                            if (Object.keys(res.body).length > 0) {
                                if(term)
                                    dispatch('doSearch',{term:term,currentItems:res.body});
                                else
                                    commit('load', res.body);
                            }
                        },
                        err => {
                            console.log(err);
                        }
                    );  
                  } else {
                    commit('message','sans succès');
                  }
                },
                err => console.log(err)
            );
        }
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};