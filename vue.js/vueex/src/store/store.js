import Vue from 'vue';
import Vuex from 'vuex';
import modSearch from './modules/modSearch';
import modProduct from './modules/modProduct';
import modCategory from './modules/modCategory';
import modSupplier from './modules/modSupplier';
import modRdBtn from './modules/modRdBtn';

Vue.use(Vuex);

export const store = new Vuex.Store({
     modules : {
         namespaced: true,
         modSearch,
         modCategory,
         modProduct,
         modSupplier,
         modRdBtn
     }
})