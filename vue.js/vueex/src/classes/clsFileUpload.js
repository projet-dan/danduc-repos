const HEIGHT = 135,
  WIDTH = 192,
  NONE = 0;

export var clsFileUpload = function() {
  this.uploadStatus = 0;
  this.height = HEIGHT;
  this.width = WIDTH;
};

clsFileUpload.prototype.UploadStatus = function(uploadStatus) {
  this.uploadStatus = uploadStatus;
};

clsFileUpload.prototype.FileToSave = function(file, model) {
  if (this.uploadStatus) {
    model.photo = file;
    this.height = HEIGHT;
    this.width = WIDTH;
  }
};

clsFileUpload.prototype.removeImage = function(model) {
  model.photo = "";
  this.height = NONE;
  this.width = NONE;
  this.uploadStatus = 0;
};
