export var clsProduct = function() {
  this.id = "";
  this.nom = "";
  this.categorie_id = "";
  this.fournisseur_id = "";
  this.fournisseur_nom = "";
  this.categorie_nom = "";
  this.quantite_par_unite = "";
  this.prix = "";
  this.quantite_en_stock = "";
  this.quantite_commande = "";
  this.reapprovisionnement = "";
  this.discontinue = "";
  this.active = "";
};
