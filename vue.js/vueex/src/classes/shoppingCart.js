/*
* Program : Classe ShoppingCart
* Écrit par : Dan Duc Dao
*/

export var ShoppingCart = function(id, quantite, prix, nom, total) {
  this.id = id;
  this.quantite = quantite;
  this.prix = prix;
  this.nom = nom;
  this.total = total;
};
