#include <iostream>
using namespace std;

int main()
{
	int scores[]{ 100, 95, 89, 73, 56, 23, 45 };

	cout << "Address of first element of array : " << scores << endl;
	cout << "Address of second element of array : " << (scores + 1) << endl;
	cout << "Address of third element of array : " << (scores + 2) << endl << endl;

	int* score_ptr{ scores };

	cout << "Value of first element of array : " << scores[0] << endl;
	cout << "Value of second element of array : " << scores[1] << endl;
	cout << "Value of third element of array : " << scores[2] << endl << endl;

	cout << "Value of first element of array : " << *scores << endl;
	cout << "Value of second element of array : " << *(scores + 1) << endl;
	cout << "Value of third element of array : " << *(scores + 2) << endl << endl;

	cout << "Address of first element of pointer : " << score_ptr << endl;
	cout << "Address of second element of pointer : " << (score_ptr + 1) << endl;
	cout << "Address of third element of pointer : " << (score_ptr + 2) << endl << endl;

	cout << "Value of first element of pointer : " << score_ptr[0] << endl;
	cout << "Value of second element of pointer : " << score_ptr[1] << endl;
	cout << "Value of third element of pointer : " << score_ptr[2] << endl << endl;

	cout << "Value of first element of pointer : " << *score_ptr << endl;
	cout << "Value of second element of pointer: " << *(score_ptr + 1) << endl;
	cout << "Value of third element of pointer : " << *(score_ptr + 2) << endl << endl;

}