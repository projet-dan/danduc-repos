#include <iostream>
#include <vector>
using namespace std;

int main()
{
	int num{ 10 };

	cout << "Value of num is : " << num << endl;
	cout << "Sizeof of num is : " << sizeof num << endl;
	cout << "Address of num is : " << &num << endl << endl;

	int *p = &num;

	cout << "Value of p is : " << *p << endl;
	cout << "Address of p is : " << p << endl;
	cout << "Sizeof of p is : " << sizeof p << endl;

	p = nullptr;

	cout << "Value of p is : " << p << endl << endl;

	string name{ "John Doe" };
	string *string_str{ &name };
	cout << "Value of pointer after(*string_str = &name): " << *string_str << endl;
	name = "George Vanier";
	cout << "Value of pointer after(name = George Vanier) :" << *string_str << endl;
	*string_str = "Macdonald";
	cout << "Value of variable after(*string_str = Macdonald)" << name << endl;
	cout << "Value of pointer after(*string_str = Macdonald)" << *string_str << endl << endl;;

	double high_temp{ 100.8 };
	double low_temp{ 37.4 };
	double *temp_ptr{ &high_temp };
	cout << "The high temperature is : " << *temp_ptr << endl;
	temp_ptr = &low_temp ;
	cout << "The low temperature : " << *temp_ptr << endl << endl;
	
	vector<string> people{ "Larry", "Moe", "Curly", "Frank", "James" };
	vector<string> *vector_ptr{ nullptr };
	vector_ptr = &people;
	cout << "First person of vector <people> : " << people.at(0) << endl;
	cout << "First person of pointer <vector_ptr> : " << (*vector_ptr).at(0) << endl << endl;
	
	for (auto people : *vector_ptr)
	{
		cout << people << endl;
	}
} 
