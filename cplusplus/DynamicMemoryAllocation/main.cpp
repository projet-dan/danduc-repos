#include <iostream>
using namespace std;

int main()
{
	int *int_ptr{ nullptr };
	int_ptr = new int;

	cout << "Address of pointer : " << int_ptr << endl;
	cout << "Value of pointer : " << *int_ptr << endl;
	*int_ptr = 100;
	cout << "Value of pointer : " << *int_ptr << endl << endl;

	delete int_ptr;

	int *array_ptr{ nullptr };
	size_t size{0};
	cout << "How big do you want the array? ";
	cin >> size;
	array_ptr = new int[size];

	cout << "Address of pointer of array : " << array_ptr << endl;

	for(int i = 0; i < size; i++)
	{
		int y = i + 1;
		*(array_ptr + i) = y;
	}

	for (int i = 0; i < size; i++)
	{
		cout << *(array_ptr + i) << " ";
	}

	delete []array_ptr;

	cout << endl;
}
