#include <iostream>
#include <regex>
#include <string>
using namespace std;

int main()
{
	string monnaie;
	regex regex_pattern("^[0-9]*.*[0-9]*$");
	char depot = 'Y';
	vector<int> monnaies;
	int prix[]{ 65,50,50,65,65,50 };
	string produit[]{ "Coke","Pepsi", "Sprit", "Diet Coke", "Diet Pepsi", "Diet Sprit" };

	cout << "0. Coke : 65 cents" << endl;
	cout << "1. Pepsi : 50 cents" << endl;
	cout << "2. Sprit : 50 cents" << endl;
	cout << "3. Diet Coke : 65 cents" << endl;
	cout << "4. Diet Pepsi : 65 cents" << endl;
	cout << "5. Diet Sprit : 50 cents" << endl;
	int boisson;
	do {
		cout << "Veuillez entrer un numero : ";
		cin >> boisson;
	} while (boisson != 0 && boisson != 1 && boisson != 2 && boisson != 3 && boisson != 4 && boisson != 5);

	do {
		cout << "Veuillez deposer votre monnaie : ";
		cin >> monnaie;
		try {
			int cents = 0;
			int pos = monnaie.find('.');
			if (regex_match(monnaie, regex_pattern) && stoi(monnaie) > 0 && monnaie.find('0') != 0)
			{
				if (pos != std::string::npos)
				{
					int dollars = stoi(monnaie.substr(0, pos));
					if (dollars >= 1)
						cents = dollars * 100;
					cents += stoi(monnaie.substr(++pos, monnaie.length() - 1));
					monnaies.push_back(cents);
				}
				else
				{
					cents = stoi(monnaie) * 100;
					monnaies.push_back(cents);
				}
				cout << "Voulez vous deposer de la monnaie encore? (Y/N) : ";
				cin >> depot;
			}
			else {
				cout << "Montant invalid" << endl;
			}
		}
		catch (exception & e)
		{
			cout << "Montant invalide" << endl;
		};

	} while (!regex_match(monnaie, regex_pattern) || depot != 'N');

	int total = 0;

	for (auto monnaie : monnaies)
	{
		total += monnaie;
	}

	int prix_produit_selection = prix[boisson];

	if (total == prix_produit_selection)
	{
		cout << "Achat d'un " << produit[boisson] << endl;
	}
	else if (total < prix_produit_selection)
	{
		cout << "Monnaie insuffisante : $" << total / 100 << endl;
	}
	else if (total > prix_produit_selection)
	{
		cout << "Achat d'un " << produit[boisson] << endl;
		int grandTotal = total - prix_produit_selection;
		cout << "Monnaie restante : $" << static_cast<double>(grandTotal) / 100 << endl;
	}

	cout << "Transaction completee! Merci et aurevoir";
}


