#include <iostream>
using namespace std;

static int fibo_recursive(int n)
{
	int i = 0;
	if (n == 0 || n == 1)
	{
		return 1;
	}
	else
	{
		return  fibo_recursive(n - 2) + fibo_recursive(n - 1);
	}
}

static int fibo_iterative(int n)
{
	int c = 1;
	int p = 1;
	if (n == 0 || n == 1)
		return 1;
	for (int i = 2; i <= n; i++)
	{
		c = p + c;
		p = c - p;
	}
	return c;
}

int main()
{
	cout << "calculer fibonacci (version récursive)" << endl;

	for (int i = 0; i < 40; i++)
	{
		cout << fibo_recursive(i) << " ";
	}

	cout << endl << endl;

	cout << "calculer fibonacci (version itératrive)" << endl;

	for (int i = 0; i < 40; i++)
	{
		cout << fibo_iterative(i) << " ";
	}

	cout << endl;
}



