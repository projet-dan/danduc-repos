#include <iostream>
#include <vector>
using namespace std;

int square(int &n)
{
	return n * n;
}

int main()
{
	int num{ 100 };
	int& ref{ num };

	cout << num << endl;
	cout << ref << endl;

	cout << endl;

	num = 200;

	cout << num << endl;
	cout << ref << endl;

	cout << endl;

	ref = 300;

	cout << num << endl;
	cout << ref << endl;

	cout << endl;

	vector<string> fruits{ "banana", "orange" , "kiwi", "strawberry", "grapes" };

	for (auto &fruit : fruits)
	{
		fruit = "peach";
	}

	for (auto& fruit : fruits)
	{
		cout << fruit << " ";
	}

	cout << endl;

	int x{ 100 };
	int& ref1 = x;
	ref1 = 200;
	
	cout << ref1 << endl;

	cout << endl;

	cout << square(num) << endl;

}
