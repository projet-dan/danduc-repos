#include <iostream>
#include <vector>
using namespace std;

void double_data(int *int_ptr)
{
	*int_ptr *= 2;
}

void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void setFruit(vector<string>* fruit_ptr, int position, string fruit)
{
	(*fruit_ptr).at(position) = fruit;
}

string getFruit(vector<string>* fruit_ptr, int position)
{
	return  (*fruit_ptr).at(position);
}

void displayFruits(const vector<string> *fruit_ptr)
{
	for (auto fruit : *fruit_ptr)
	{
		cout << fruit << " ";
	}
}

int *largest_number(int *int_ptr1,int *int_ptr2)
{
	if (*int_ptr1 > * int_ptr2)
	{
		return int_ptr1;
	}
	else if (*int_ptr1 < *int_ptr2)
	{
		return int_ptr2;
	}
}

int *create_array(size_t size, int init_value = 0)
{
	int *new_int_ptr{ nullptr };
	new_int_ptr = new int[size];
	for (size_t i = 0; i < size; i++)
	{
		*(new_int_ptr + i) = init_value;
	}
	return new_int_ptr;
}

void displayArray(int *array, size_t size)
{
	for(int i = 0; i < size; i++)
	{
		cout << array[i] << " ";
	}
}

int main()
{
	int number{ 10 };

	cout << "Number before : " << number << endl;
	double_data(&number);
	cout << "Number after : " << number << endl;

	cout << endl;

	int* int_ptr{ nullptr };
	int_ptr = &number;

	cout << "Value of pointer before : " << *int_ptr << endl;
	double_data(int_ptr);
	cout << "Value of pointer after : " << *int_ptr << endl;

	cout << endl;

	int x{ 100 };
	int y{ 200 };
	cout << "x before : " << x << endl;
	cout << "y before : " << y << endl;
	swap(&x, &y);
	cout << "x after : " << x << endl;
	cout << "y after : " << y << endl;

	cout << endl;

	cout << "Fruits before : " << endl;
	vector<string> fruits{ "banana", "orange" , "kiwi", "strawberry", "grapes" };
	displayFruits(&fruits);

	cout << endl;

	setFruit(&fruits, 1, "peach");
	cout << "Fruits after : " << endl;
	displayFruits(&fruits);

	cout << endl;
	int pos = 3;
	cout << "Fruit found at position " << pos << " is : " << getFruit(&fruits, pos) << endl;

	cout << endl;

	int a{ 100 };
	int b{ 200 };

	int *result_ptr{ nullptr };

	result_ptr = largest_number(&a, &b);
	cout << "The largest number is : " << *result_ptr;

	cout << endl;

	result_ptr = nullptr;

	int size = 4;
	int value = 100;
	result_ptr = create_array(size, value);
	displayArray(result_ptr, size);

	delete []result_ptr;

	cout << endl;
}
