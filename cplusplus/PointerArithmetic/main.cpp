#include <iostream>
using namespace std;

int main()
{
	int scores[]{ 100, 95, 83,200, 103, 300, 50 , -1};
	int *score_ptr{ scores };

	while (*score_ptr != -1)
	{
		cout << *score_ptr << " ";
		score_ptr++;
	}

	cout << endl;

	score_ptr = scores;

	while (*score_ptr != -1)
	{
		cout << *score_ptr++ << " ";
	}

	cout << endl;

	scores[0] = 10;
	scores[1] = 15;
	scores[2] = 45;
	scores[3] = 99;

	score_ptr = scores;

	for (size_t i = 0 ; i < sizeof(score_ptr); i++)
	{
		cout << score_ptr[i] << " ";
	}
	
	cout << endl;

	scores[0] = 56;
	scores[1] = 78;
	scores[2] = 67;
	scores[3] = 23;

	score_ptr = scores;

	for (size_t i = 0; i < size(scores); i++)
	{
		cout << scores[i] << " ";
	}

	cout << endl << endl;

	string s1{ "Frank" };
	string s2{ "Frank" };
	string s3{ "John" };

	string* p1{ &s1 };
	string* p2{ &s2 };
	string* p3{ &s1 };

	cout << "Compare address of pointer p1 & p2. " << p1 << " == " << p2 << " : " << (p1 == p2) << endl;
	cout << "Compare address of pointer p1 & p3. " << p1 << " == " << p3 << " : " << (p1 == p3) << endl;
	cout << "Compare value of pointer *p1 & *p2. " << *p1 << " == " << *p2 << " : " << (*p1 == *p2) << endl;
	cout << "Compare value of pointer *p1 & *p3. " << *p1 << " == " << *p3 << " : " << (*p1 == *p3) << endl;

	cout << endl;

	p3 = &s3;
	cout << "Compare value of pointer *p1 & *p3. " << *p1 << "==" << *p3 << " : " << (*p1 == *p3) << endl;

	cout << endl;

	char name[]{ "John Doe" };
	char *char_ptr1{ nullptr };
	char *char_ptr2{ nullptr };
	char_ptr1 = name;
	char_ptr2 = &name[3];
	cout << "In the string" << name << ", " << *char_ptr2 << " is " << (char_ptr2 - char_ptr1) << " characters away from " << *char_ptr1 << endl;
}


