#include <iostream>
#include <string>
#include <regex>

using namespace std;

int main()
{
	char letter;
	string val1;
	string val2;
	int val3;
	char q;
	regex regex_pattern("-?[0-9]+.?[0-9]+");
	do {
		do {
			cout << "Entrer un caract�re (+,-,*,/) : ";
			cin >> letter;
		} while (letter != '+' && letter != '-' && letter != '*' && letter != '/');

		do {
			cout << "Entrer le premier op�rande : ";
			cin >> val1;
		} while (!regex_match(val1, regex_pattern));

		do {
			cout << "Entrer le deuxi�me op�rande : ";
			cin >> val2;
		} while (!regex_match(val2, regex_pattern));

		cout << "Voulez-vous continuer? R�pondre par y ou n : ";
		cin >> q;

		if (letter == '+')
		{
			val3 = atoi(val1.c_str()) + atoi(val2.c_str());
			cout << "L'addition de " << val1 << " et " << val2 << " donnerait " << val3 << endl;
		}
		else if (letter == '-')
		{
			val3 = atoi(val1.c_str()) - atoi(val2.c_str());
			cout << "La soustraction de " << val1 << " et " << val2 << " donnerait " << val3 << endl;
		}
		else if (letter == '*')
		{
			val3 = atoi(val1.c_str()) * atoi(val2.c_str());
			cout << "La multiplication de " << val1 << " et " << val2 << " donnerait " << val3 << endl;
		}
		else if (letter == '/')
		{
			val3 = atoi(val1.c_str()) / atoi(val2.c_str());
			cout << "La division de " << val1 << " et " << val2 << " donnerait " << val3 << endl;
		}
	} while(tolower(q) != 'y');

	cout << endl;
}
