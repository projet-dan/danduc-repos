﻿using System;
using System.IO;

namespace ArrayFindAll
{
    class Program
    {
        private static bool CheckForSmallFile(FileInfo file)
        {
            return file.Length < 1000;
        }

        private static void DisplayFileInfo(FileInfo[] files)
        {
            foreach (FileInfo file in files)
                Console.WriteLine("{0} ({1} byte(s))", file.Name, file.Length);
        }

        static void Main(string[] args)
        {
            FileInfo[] files = new DirectoryInfo("c:\\").GetFiles();

            if (files.Length > 0)
            {
                FileInfo[] outputFiles = Array.FindAll(files, CheckForSmallFile);
                if (outputFiles.Length > 0)
                {
                    DisplayFileInfo(outputFiles);
                }
                else
                {
                    Console.WriteLine("No files have length less than 1000");
                }
            }
            else
            {
                Console.WriteLine("Files not found in c drive");
            }
        }
    }
}
