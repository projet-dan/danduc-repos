﻿using System;
using ConsoleAppLibrary.Functions;

namespace GetProductsSoldByProduct
{
    class Program
    {
        static void Main(string[] args)
        {
            Function func = new Function();
            Console.WriteLine(func.GetProductsSoldByProduct(3));
        }
    }
}
