﻿using System;
using ConsoleAppLibrary;
using ConsoleAppLibrary.Functions;

namespace GetProductStatistic
{
    class Program
    {
        static void Main(string[] args)
        {
            Function func = new Function();
            Statistic stats = func.GetProductStatistic();

            Console.WriteLine("Number item : {0:G}", stats.numberItems);
            Console.WriteLine("Unit Sold : {0:G}", stats.unitySold);
            Console.WriteLine("Total Sales : {0,2:C}", stats.totalSales);
            Console.WriteLine("Average Price : {0,2:C}", stats.averagePrice);
            
        }
    }
}
