﻿CREATE PROCEDURE dbo.ListEmployees
AS
BEGIN
	SET NOCOUNT ON;
	SELECT EmployeeID, LastName, FirstName
	FROM dbo.Employees;
END
GO

EXEC dbo.ListEmployees
GO

CREATE PROCEDURE dbo.ListEmployeesByCity
	@City varchar(25)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT EmployeeID, LastName, FirstName
	FROM dbo.Employees
	WHERE City = @City
END
GO

EXEC dbo.ListEmployeesByCity @City = 'London'
EXEC dbo.ListEmployeesByCity 'London'
GO

CREATE PROCEDURE dbo.ListEmployeesByOptionalCity
	@City varchar(25) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @City IS NULL
		SELECT EmployeeID, LastName, FirstName
		FROM dbo.Employees
	ELSE
		SELECT EmployeeID, LastName, FirstName
		FROM dbo.Employees
		WHERE City = @City
END
GO

EXEC dbo.ListEmployeesByOptionalCity @City = 'London'
EXEC dbo.ListEmployeesByOptionalCity
GO

CREATE PROCEDURE dbo.InsertShipper
	@CompanyName nvarchar(40) = NULL,
	@Phone nvarchar(24) = NULL,
	@ShipperID int = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	IF @CompanyName IS NULL
		SET @ShipperID = 0;
	ELSE
	    BEGIN
			INSERT INTO dbo.Shippers(CompanyName, Phone)
			VALUES(@CompanyName,@Phone);
			SET @ShipperID = SCOPE_IDENTITY();
		END
END
GO

DECLARE @NewShipperID int;
EXEC dbo.InsertShipper
     @CompanyName = 'Northwind',
     @Phone = NULL,
     @ShipperID = @NewShipperID
SELECT @NewShipperID AS ShipperID
GO

CREATE PROCEDURE [dbo].[InsertShipperReturn]
	@CompanyName nvarchar(40) = NULL,
	@Phone nvarchar(24) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @CompanyName IS NULL
	   RETURN 0;
	ELSE
	    BEGIN
	        DECLARE @NewId int;
			INSERT INTO dbo.Shippers(CompanyName, Phone)
			VALUES(@CompanyName,@Phone);
			SET @NewId = SCOPE_IDENTITY();
			RETURN @NewId;
		END
END

DECLARE @NewShipperId int;
EXEC @NewShipperId = dbo.InsertShipperReturn
      @CompanyName = 'Jonh Doe',
      @Phone = NULL;
SELECT @NewShipperId AS ShipperId


