﻿using System;
using ConsoleAppLibrary;

namespace SortByWriter
{
    class Program
    {
        private static void DisplayWriters(Writer[] writers)
        {
            foreach (var writer in writers)
            {
                Console.WriteLine("{0} - {1}", writer.Name, writer.HomeState);
            }
        }

        static void Main(string[] args)
        {
            WriterSortable[] writerSortable = new WriterSortable[4];
            writerSortable[0] = new WriterSortable("Robert", "WA");
            writerSortable[1] = new WriterSortable("Mary", "FL");
            writerSortable[2] = new WriterSortable("Andy", "FL");
            writerSortable[3] = new WriterSortable("Ken", "CA");
            try
            {
                Array.Sort(writerSortable);
                DisplayWriters(writerSortable);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Sort failed: " + ex.Message);
            }
        }
    }
}
