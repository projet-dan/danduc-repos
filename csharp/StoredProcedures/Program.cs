﻿using ConsoleAppLibrary.StoredProcedures;
using ConsoleAppLibrary;
using System.Collections.Generic;
using System;

namespace StoredProcedures
{
    class Program
    {
        static void Main(string[] args)
        {
            StoredProcedure sp = new StoredProcedure();
            List<Employees> employees = sp.GetListEmployees();

            foreach (var employee in employees)
            {
                Console.WriteLine("-{0} {1} ( {2} )", employee.FirstName, 
                                                      employee.LastName,
                                                      employee.ID.ToString());
            }

            Console.WriteLine();

            employees = sp.GetListEmployeesByCity();
            foreach (var employee in employees)
            {
                Console.WriteLine("-{0} {1} ( {2} )", employee.FirstName,
                                                      employee.LastName,
                                                      employee.ID.ToString());
            }
        }
    }
}
