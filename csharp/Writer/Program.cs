﻿using System;
using ConsoleAppLibrary;

namespace AddWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            Writer1 writer1 = new Writer1();
            writer1.Add(new Writer("Andy", "WA"));
            writer1.Add(new Writer("Mary", "FL"));
            writer1.Add(new Writer("Robert", "FL"));
            writer1.Add(new Writer("Ken", "CA"));
            writer1.Add(new Writer("Doug", "CA"));

            Writer2 writer2 = new Writer2();
            writer2.Add(new Writer("Andy", "WA"));
            writer2.Add(new Writer("Mary", "FL"));
            writer2.Add(new Writer("Robert", "FL"));
            writer2.Add(new Writer("Ken", "CA"));
            writer2.Add(new Writer("Doug", "CA"));

            try
            {
                Console.WriteLine(writer2[0]);
                Console.WriteLine(writer2["Doug"].HomeState);
                writer2["Andy"] = new Writer("Andy", "NY");
                for (int i = 0; i < writer2.Length; i++)
                {
                    Console.WriteLine(writer2[i]);
                }
                Console.WriteLine();
                Console.WriteLine(writer2[17]);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add writer failed : " + ex.Message);
            }
        }
    }
}
