﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppLibrary
{
    public class Orders
    {
        public int ID
        {
            get;set;
        }

        public DateTime OrderDate
        {
            get;
            set;
        }

        public string ShipCountry
        {
            get;
            set;
        }

        public string CustomerId
        {
            get;
            set;
        }

        public int OrderId
        {
            get;
            set;
        }

        public int EmployeeId
        {
            get;
            set;
        }
    }
}
