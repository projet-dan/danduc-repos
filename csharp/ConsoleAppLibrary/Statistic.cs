﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppLibrary
{
    public struct Statistic
    {
        public int unitySold;
        public decimal totalSales;
        public decimal averagePrice;
        public int numberItems;
    }
}
