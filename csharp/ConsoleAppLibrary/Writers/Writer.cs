﻿namespace ConsoleAppLibrary
{
    public class Writer
    {
        public string Name = string.Empty;
        public string HomeState = string.Empty;

        public Writer() { }

        public Writer(string Name, string HomeState)
        {
            this.Name = Name;
            this.HomeState = HomeState;
        }

        public override string ToString()
        {
            return string.Format("{0} is from {1}", this.Name, this.HomeState);
        }
    }
}
