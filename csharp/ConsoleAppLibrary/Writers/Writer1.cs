﻿using System;

namespace ConsoleAppLibrary
{
    public class Writer1
    {
        private int currentSize = 4;
        private int writerCount = 0;
        private Writer[] writers;

        public Writer1()
        {
            writers = new Writer[currentSize];
        }

        public void Add(Writer newWriter)
        {
            if (writerCount >= writers.Length)
            {
                currentSize *= 2;
                Writer[] temp = (Writer[])writers.Clone();
                writers = new Writer[currentSize];
                Array.Copy(temp, writers, temp.Length);
                temp = null;
            }
            writers[writerCount++] = newWriter;
        }

        public int Length
        {
            get { return writerCount; }
        }
    }
}
