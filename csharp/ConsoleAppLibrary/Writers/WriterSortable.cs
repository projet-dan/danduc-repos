﻿using System;
using System.Collections;

namespace ConsoleAppLibrary
{
    public class WriterSortable : Writer, IComparable
    {
        public WriterSortable() { }

        public WriterSortable(string Name, string HomeState) : base(Name, HomeState)
        { }

        public int CompareTo(object obj)
        {
            WriterSortable otherWriter = (WriterSortable)obj;
            return this.Name.CompareTo(otherWriter.Name);
        }
    }
}
