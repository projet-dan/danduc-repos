﻿using System.Collections;
using System;

namespace ConsoleAppLibrary
{
    public class Writer3 : IEnumerable
    {
        private int currentSize = 4;
        private int writerCount = 0;
        private Writer[] writers;

        public Writer3()
        {
            writers = new Writer[currentSize];
        }

        public void Add(Writer newWriter)
        {
            if (writerCount >= writers.Length)
            {
                currentSize *= 2;
                Writer[] temp = (Writer[])writers.Clone();
                writers = new Writer[currentSize];
                Array.Copy(temp, writers, temp.Length);
                temp = null;
            }
            writers[writerCount++] = newWriter;
        }

        public int Length
        {
            get { return writerCount; }
        }

        public Writer this[int index]
        {
            get
            {
                if (index < 0 || index >= writers.Length)
                {
                    throw new IndexOutOfRangeException();
                }
                return writers[index];
            }
            set
            {
                if (index < 0 || index >= writerCount)
                {
                    throw new IndexOutOfRangeException();
                }
                writers[index] = value;
            }
        }

        public Writer this[string writerName]
        {
            get
            {
                foreach (Writer item in writers)
                {
                    if (item.Name == writerName)
                    {
                        return item;
                    }
                }
                throw new IndexOutOfRangeException("Unknow writer name 3");
            }
            set
            {
                for (int i = 0; i < writerCount; i++)
                {
                    if (writers[i].Name == writerName)
                    {
                        writers[i] = value;
                        return;
                    }
                }
                throw new IndexOutOfRangeException("Unknow writer name 5");
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Writer item in writers)
            {
                yield return item;
            }
        }
    }
}
