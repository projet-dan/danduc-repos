﻿using System;

namespace ConsoleAppLibrary
{
    public class State
    {
        public static string[] States
        {
            get
            {
                return new string[] {
                       "Alaska",
                       "Alabama",
                       "Arkahsas",
                       "Arizona",
                       "California",
                       "Colorado",
                       "Connecticut",
                       "Washington, D.C",
                       "Delaware",
                       "Florida",
                       "Georgia",
                       "Guam",
                       "Hawaii",
                       "Iowa",
                       "Idaho",
                       "Illinois",
                       "Indiana",
                       "Kansas",
                       "Kentucky",
                       "Louisiana",
                       "Massachusetts",
                       "Maryland",
                       "Maine",
                       "Michigan",
                       "Minnesota",
                       "Missouri",
                       "Mississipi",
                       "Montana",
                       "North Carolina",
                       "North Dakota",
                       "Nebraska",
                       "New Hampshire",
                       "New Jersey",
                       "New Mexico",
                       "Nevada",
                       "New York",
                       "Ohio",
                       "Oklahoma",
                       "Oregon",
                       "Pennsylvania",
                       "Puerto Rico",
                       "Rhode Island",
                       "South Carolina",
                       "South Dakota",
                       "Tennessee",
                       "Texas",
                       "Utah",
                       "Virginia",
                       "Vermont",
                       "Washington",
                       "Wisconsin",
                       "West Virginia",
                       "Wyoming"
                    };
            }
        }
    }
}
