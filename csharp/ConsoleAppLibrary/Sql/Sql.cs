﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace ConsoleAppLibrary
{
    public class Sql
    {
        private SqlConnectionStringBuilder builder;

        public Sql()
        {
            builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = "Server=127.0.0.1;Database=Northwind;Trusted_Connection=True;";
        }

        public void GetCustomerInfo()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();

                    sb.Append("SELECT TOP 10 CustomerID, CompanyName ");
                    sb.Append("FROM [dbo].[Customers]");

                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
        }
        public List<Orders> GetOrdersWithPriceUnder3Dollars()
        {
            List<Orders> orders = new List<Orders>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();

                    sb.Append("SELECT OrderID, OrderDate " +
                              "FROM dbo.Orders AS O " +
                              "WHERE EXISTS  (SELECT OD.OrderID " +
                                              "FROM dbo.[Order Details] AS OD " +
                                              "WHERE O.OrderID = OD.OrderID " +
                                              "AND OD.UnitPrice < 3)");
                 
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var order = new Orders();
                                order.ID = reader.GetInt32(0);
                                order.OrderDate = reader.GetDateTime(1);
                                orders.Add(order);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return orders;
        }

        public void GetCategoryAverage()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();

                    sb.Append("WITH CategoryAverages(CategoryID, AvgPrice) " +
                              "AS (" +
                                      "SELECT CategoryID, AVG(UnitPrice) AS AvgPrice " +
                                      "FROM dbo.Products " +
                                      "GROUP BY CategoryID " +
                                  ") " +
                               "SELECT P.ProductName, P.UnitPrice, CA.AvgPrice, C.CategoryName " +
                               "FROM dbo.Products AS P " +
                               "INNER JOIN CategoryAverages AS CA " +
                               "ON CA.CategoryID = P.CategoryID " +
                               "INNER JOIN dbo.Categories AS C " +
                               "ON P.CategoryID = C.CategoryID " +
                               "ORDER BY P.UnitPrice DESC");

                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("Product : {0}", reader.GetString(0).ToString());
                                Console.WriteLine("Price : ${0}", Math.Round(reader.GetDecimal(1),2).ToString());
                                Console.WriteLine("Average : ${0}", Math.Round(reader.GetDecimal(2),2).ToString());
                                Console.WriteLine("Category : {0} \n", reader.GetString(3));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
        }
        public void GetCustomersWithMostOrders()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();

                    sb.Append("SELECT TOP (10) WITH TIES C.CustomerID, C.CompanyName, COUNT(*) AS NumOrders " +
                               "FROM dbo.Customers AS C " +
                               "INNER JOIN dbo.Orders AS O " +
                               "ON C.CustomerID = O.CustomerID " +
                               "GROUP BY C.CustomerID, C.CompanyName " +
                               "ORDER BY NumOrders DESC");

                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("-{0} order(s) of {1} ({2})", reader.GetInt32(2).ToString(), reader.GetString(1), reader.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
        }
    }
}
