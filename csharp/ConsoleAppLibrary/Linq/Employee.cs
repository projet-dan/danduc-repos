﻿using System;

namespace ConsoleAppLibrary
{
    class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime HireDate { get; set; }
    }
}
