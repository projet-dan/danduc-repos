﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace ConsoleAppLibrary
{
    public class Linq
    {
        public static void QueryAge()
        {
            int[] ages = { 2, 21, 40, 72, 100 };
            IEnumerator enumerator = ages.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine((int)enumerator.Current);
            }
        }
        public static void QueryInstructors()
        {
            string[] instructors = { "Aaron", "Fritz", "Paul", "Scott", "Jonh", "Sharon" };
            IEnumerable<string> query = from s in instructors
                                        where s.Length == 5
                                        orderby s descending
                                        select s;
            foreach (string name in query)
            {
                Console.WriteLine(name);
            }
        }
        public static void QueryEmployees()
        {
            IEnumerable<Employee> employees = new List<Employee>()
            {
                new Employee { ID = 1, Name = "Scott", HireDate = new DateTime(2002,3,5) },
                new Employee { ID = 2, Name = "Daniel", HireDate = new DateTime(2002,10,15) },
                new Employee { ID = 3, Name = "Paul", HireDate = new DateTime(2007,10,11) }
            };

            IEnumerable<Employee> queryEmployees = from e in employees
                                                   where e.HireDate.Year < 2005
                                                   orderby e.Name
                                                   select e;
            foreach (Employee employee in queryEmployees)
            {
                Console.WriteLine(employee.Name);
            }
        }
        //LINQ  to Objects
        public static void QueryGetProcesses()
        {

            IEnumerable<Process> processList = from p in Process.GetProcesses()
                                               where String.Equals(p.ProcessName, "svchost")
                                               orderby p.WorkingSet64 descending
                                               select p;
            foreach (Process process in processList)
            {
                Console.WriteLine(string.Format("{0} - {1}", process.Id, process.ProcessName));
            }
        }
    }
}
