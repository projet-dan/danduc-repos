﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppLibrary
{
    public class Employees
    {
        public int ID
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public string FirstName
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }

        public DateTime BirthDate
        {
            get;
            set;
        }
    }

}
