﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleAppLibrary.Functions
{
    public class Function
    {
        private SqlConnectionStringBuilder builder;

        public Function()
        {
            builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = "Server=127.0.0.1;Database=Northwind;Trusted_Connection=True;";
        }

        public int GetProductsSold()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT dbo.fnProductsSold()", connection))
                    {
                        return (int)cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return 0;
        }

        public int GetProductsSoldByProduct(int productId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT dbo.fnProductsSoldByProduct(@ProductId)", connection))
                    {
                        cmd.Parameters.Add(new SqlParameter("@ProductId", productId));
                        return (int)cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return 0;
        }

        public Statistic GetProductStatistic()
        {
            Statistic stats = new Statistic();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.fnProductStatistics()", connection))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                stats.unitySold = reader.GetInt32(0);
                                stats.totalSales = reader.GetDecimal(1);
                                stats.averagePrice = reader.GetDecimal(2);
                                stats.numberItems = reader.GetInt32(3);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return stats;
        }

        public Statistic GetStatisticByProduct(int productId)
        {
            Statistic stats = new Statistic();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.fnStatisticsByProduct(@ProductId)", connection))
                    {
                        cmd.Parameters.Add(new SqlParameter("@ProductId", productId));

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                stats.unitySold = reader.GetInt32(0);
                                stats.totalSales = reader.GetDecimal(1);
                                stats.numberItems = reader.GetInt32(2);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return stats;
        }

        public List<Orders> GetOrdersByEmployeeCountry(int? employeeId, string shipCountry)
        {
            List<Orders> orders = new List<Orders>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.fnOrdersByEmployee_Country(@EmployeeId,@ShipCountry)", connection))
                    {
                        if(employeeId == null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                        }else
                        {
                            cmd.Parameters.Add(new SqlParameter("@EmployeeId", employeeId));
                        }
                        
                        cmd.Parameters.Add(new SqlParameter("@ShipCountry", shipCountry));

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Orders order = new Orders();
                                order.ShipCountry = reader.GetString(0);
                                order.CustomerId = reader.GetString(1);
                                order.OrderId = reader.GetInt32(2);
                                order.EmployeeId = reader.GetInt32(3);
                                orders.Add(order);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }

            return orders;
        }

    }
}
