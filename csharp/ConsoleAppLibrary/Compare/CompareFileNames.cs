﻿using System.Collections;
using System.IO;

namespace ConsoleAppLibrary
{
    public class CompareFileNames : IComparer
    {
        public int Compare(object x, object y)
        {
            FileInfo file1 = (FileInfo)x;
            FileInfo file2 = (FileInfo)y;
            return file1.FullName.CompareTo(file2.FullName);
        }
    }
}
