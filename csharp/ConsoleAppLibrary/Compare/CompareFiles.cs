﻿using System.Collections;
using System.IO;

namespace ConsoleAppLibrary
{
    public class CompareFiles : IComparer
    {
        public enum CompareField
        {
            Name,
            Length
        };

        public CompareField CompareOn = CompareField.Name;

        public CompareFiles(CompareField compareOn)
        {
            this.CompareOn = compareOn;
        }

        public int Compare(object x, object y)
        {
            FileInfo file1 = (FileInfo)x;
            FileInfo file2 = (FileInfo)y;
            int result = 0;
            switch (this.CompareOn)
            {
                case CompareField.Name:
                    result = file1.FullName.CompareTo(file2.FullName);
                    break;
                case CompareField.Length:
                    result = file1.Length.CompareTo(file2.Length);
                    if (result == 0)
                        result = file1.FullName.CompareTo(file2.FullName);
                    break;
            }
            return result;
        }
    }
}
