﻿using System.Collections;
using System.IO;

namespace ConsoleAppLibrary
{
    public class CompareFileLengths : IComparer
    {
        public int Compare(object x, object y)
        {
            FileInfo file1 = (FileInfo)x;
            FileInfo file2 = (FileInfo)y;

            int result = file1.Length.CompareTo(file2.Length);

            if (result == 0)
            {
                result = file1.FullName.CompareTo(file2.FullName);
            }
            return result;
        }
    }
}
