﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleAppLibrary.StoredProcedures
{
    public class StoredProcedure
    {
        private SqlConnectionStringBuilder builder;

        public StoredProcedure()
        {
            builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = "Server=127.0.0.1;Database=Northwind;Trusted_Connection=True;";
        }

        public List<Employees> GetListEmployees()
        {
            List<Employees> employees = new List<Employees>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("dbo.ListEmployees", connection))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Employees emp = new Employees();
                                emp.ID = Convert.ToInt32(reader["EmployeeID"]);
                                emp.LastName = reader["LastName"].ToString();
                                emp.FirstName = reader["FirstName"].ToString();
                                employees.Add(emp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
            return employees;  
        }

        public List<Employees> GetListEmployeesByCity()
        {
            List<Employees> employees = new List<Employees>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("dbo.ListEmployeesByCity", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@City","London"));

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Employees emp = new Employees();
                                emp.ID = Convert.ToInt32(reader["EmployeeID"]);
                                emp.LastName = reader["LastName"].ToString();
                                emp.FirstName = reader["FirstName"].ToString();
                                employees.Add(emp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
            return employees;
        }

        public int InnsertShipper(string companyName, string phone)
        {
            int lastInsertedShipperID = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
     
                    using (SqlCommand cmd = new SqlCommand("dbo.InsertShipper", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@CompanyName", companyName));
                        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
                        SqlParameter outParam = new SqlParameter();
                        outParam.ParameterName = "@ShipperID";
                        outParam.Direction = ParameterDirection.Output;
                        outParam.SqlDbType = SqlDbType.Int;

                        cmd.Parameters.Add(outParam);

                        cmd.ExecuteNonQuery();

                        lastInsertedShipperID = (int)cmd.Parameters["@ShipperID"].Value;

                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
            return lastInsertedShipperID;
        }
        public int InnsertShipperReturn(string companyName, string phone)
        {
            int lastInsertedShipperID = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("dbo.InsertShipperReturn", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@CompanyName", companyName));
                        cmd.Parameters.Add(new SqlParameter("@Phone", phone));
                        SqlParameter outParam = new SqlParameter();
                        outParam.ParameterName = "@ShipperID";
                        outParam.Direction = ParameterDirection.ReturnValue;
                        outParam.SqlDbType = SqlDbType.Int;

                        cmd.Parameters.Add(outParam);

                        cmd.ExecuteNonQuery();

                        lastInsertedShipperID = (int)cmd.Parameters["@ShipperID"].Value;

                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Connection database fail\n");
                Console.WriteLine(e.ToString());
            }
            return lastInsertedShipperID;
        }
        }
 }
