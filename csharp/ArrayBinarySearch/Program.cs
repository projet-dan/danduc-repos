﻿using System;
using ConsoleAppLibrary;

namespace ArrayBinarySearch
{
    class Program
    {
        static void Main(string[] args)
        {
            string stateToFind;
            do
            {
                Console.Write("Please enter a state to find (or press Enter to quit) : ");
                stateToFind = Console.ReadLine();
                if (!string.IsNullOrEmpty(stateToFind))
                {
                    string[] states = State.States;
                    Array.Sort(states);
                    int pos = Array.BinarySearch(states, stateToFind);
                    if (pos < 0)
                        Console.WriteLine("'{0}' wasn't found", stateToFind);
                    else
                        Console.WriteLine("'{0}' was found at postion {1}", stateToFind , Convert.ToString(pos + 1));
                }
            } while (!String.IsNullOrEmpty(stateToFind));
        }
    }
}
