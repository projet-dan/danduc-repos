﻿using System;
using System.IO;
using ConsoleAppLibrary;

namespace SortByFileName
{
    class Program
    {
        private static void DisplayFileInfo(FileInfo[] files)
        {
            foreach (FileInfo file in files)
                Console.WriteLine("{0} ({1} byte(s))", file.Name, file.Length);
        }

        static void Main(string[] args)
        {
            FileInfo[] files = new DirectoryInfo("c:\\").GetFiles();

            if (files.Length > 0)
            {
                Array.Sort(files, new CompareFileNames());
                DisplayFileInfo(files);
            }
            else
            {
                Console.WriteLine("No files found in c drive");
            }
        }
    }
}
