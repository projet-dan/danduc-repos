﻿using System;
using ConsoleAppLibrary;

namespace SortByWriterState
{
    class Program
    {
        private static int CompareWriterHome(Writer writer1, Writer writer2)
        {
            return writer1.HomeState.CompareTo(writer2.HomeState);
        }

        private static void DisplayWriters(Writer[] writers)
        {
            foreach (var writer in writers)
            {
                Console.WriteLine("{0} - {1}", writer.Name, writer.HomeState);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                Writer[] writers = new Writer[4];
                writers[0] = new Writer("Robert", "WA");
                writers[1] = new Writer("Mary", "FL");
                writers[2] = new Writer("Andy", "FL");
                writers[3] = new Writer("Ken", "CA");

                Array.Sort(writers, CompareWriterHome);
                DisplayWriters(writers);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Sort failed: " + ex.Message);
            }
            
        }
    }
}
