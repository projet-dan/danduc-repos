﻿using System;
using System.IO;
using ConsoleAppLibrary;

namespace SortByFileNameAndLength
{
    class Program
    {
        private static void DisplayFileInfo(FileInfo[] files)
        {
            foreach (FileInfo file in files)
                Console.WriteLine("{0} ({1} byte(s))", file.Name, file.Length);
        }

        static void Main(string[] args)
        {
            FileInfo[] files = new DirectoryInfo("c:\\").GetFiles();

            if (files.Length > 0)
            {
                Console.WriteLine("====Sort on file name====\n");
                Array.Sort(files, new CompareFiles(CompareFiles.CompareField.Name));
                DisplayFileInfo(files);

                Console.WriteLine("\n====Sort on file length====\n");
                Array.Sort(files, new CompareFileLengths());
                DisplayFileInfo(files);

                Console.WriteLine("\n====Sort on file length====\n");
                Array.Sort(files, new CompareFiles(CompareFiles.CompareField.Length));
                DisplayFileInfo(files);
            }
            else
            {
                Console.WriteLine("No files found in c drive");
            }
        }
    }
}
