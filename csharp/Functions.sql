﻿CREATE FUNCTION dbo.fnProductsSold() 
	RETURNS int
AS
BEGIN
	DECLARE @Total AS int;
	SELECT @Total = SUM(Quantity)
	    FROM dbo.[Order Details];
	RETURN @Total;
END
GO

SELECT dbo.fnProductsSold()
GO

CREATE FUNCTION dbo.fnProductsSoldByProduct
( 
	@ProductId int = NULL
) 
	RETURNS int
	WITH RETURNS NULL ON NULL INPUT
AS
BEGIN
	DECLARE @Total AS int;
	SELECT @Total = SUM(Quantity)
	    FROM dbo.[Order Details]
	WHERE ProductID = @ProductId
	RETURN @Total;
END
GO

SELECT dbo.fnProductsSoldByProduct(3)
GO

CREATE FUNCTION dbo.fnProductStatistics()
	RETURNS TABLE 
AS
RETURN 
(
	SELECT dbo.fnProductsSold() AS UnitSold,
	       SUM(UnitPrice * Quantity) AS TotalSales,
	       AVG(UnitPrice) AS AveragePrice,
	       COUNT(*) AS LineItems
	 FROM dbo.[Order Details]
)
GO

SELECT * FROM dbo.fnProductStatistics()
GO

ALTER FUNCTION [dbo].[fnStatisticsByProduct]
(
	@ProductId int = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT dbo.fnProductsSoldByProduct(@ProductId) AS UnitsSold,
	       SUM(UnitPrice * Quantity) AS TotalSales,
	       COUNT(*) AS LineItems
	FROM dbo.[Order Details]
	WHERE ProductID = @ProductId	
)
GO

SELECT * FROM dbo.fnStatisticsByProduct(3)
GO

ALTER FUNCTION dbo.fnOrdersByEmployee_Country
(
	@EmployeeId int = NULL,
	@ShipCountry nvarchar(15) = NULL
)
RETURNS @Order TABLE 
(
	ShipCountry nvarchar(15) NOT NULL,
	CustomerId nchar(5) NOT NULL,
	OrderId int NOT NULL,
	EmployeeId int NOT NULL
)
AS
BEGIN
    IF @EmployeeId IS NULL AND @ShipCountry IS NULL
       INSERT @Order
	   SELECT ShipCountry, CustomerID, OrderID, EmployeeID
	   FROM dbo.Orders
	   ORDER BY ShipCountry,CustomerID,OrderID,EmployeeID;
     ELSE IF @EmployeeId IS NULL AND @ShipCountry IS NOT NULL
        INSERT @Order
        SELECT ShipCountry, CustomerID, OrderID, EmployeeID
        FROM dbo.Orders
        WHERE ShipCountry = @ShipCountry
        ORDER BY EmployeeID, CustomerID,OrderID;
     ELSE IF @EmployeeId IS NOT NULL AND @ShipCountry IS NULL
        INSERT @Order
        SELECT ShipCountry, CustomerID, OrderID, EmployeeID
        FROM dbo.Orders
        WHERE EmployeeID = @EmployeeId
        ORDER BY ShipCountry, CustomerID, OrderID;
     ELSE IF @EmployeeId IS NOT NULL AND @ShipCountry IS NOT NULL
         INSERT @Order
         SELECT ShipCountry, CustomerID,OrderID,EmployeeID
         FROM dbo.Orders
         WHERE ShipCountry = @ShipCountry AND EmployeeID = @EmployeeId
         ORDER BY ShipCountry,CustomerID, OrderID,EmployeeID;  
	RETURN 
END
GO

DECLARE @EmployeeId nchar(5) = NULL;
DECLARE @ShipCountry nvarchar(15) = NULL;

SELECT ShipCountry, CustomerId, OrderId,EmployeeId
FROM  dbo.fnOrdersByEmployee_Country(@EmployeeId,@ShipCountry);

SELECT ShipCountry, CustomerId, OrderId
FROM  dbo.fnOrdersByEmployee_Country(6,NULL);

SELECT CustomerId, EmployeeId, OrderId
FROM  dbo.fnOrdersByEmployee_Country(NULL,'Spain')
ORDER BY CustomerId,EmployeeId;


