﻿using System;
using System.Collections.Generic;
using ConsoleAppLibrary;
using ConsoleAppLibrary.Functions;
using System.Linq;

namespace GetOrdersByEmployeeCountry
{
    class Program
    {
        static void Main(string[] args)
        {
            Function func = new Function();

            List<Orders> orders = func.GetOrdersByEmployeeCountry(null, "Spain");

            var query = orders.OrderBy(x => x.CustomerId).ThenBy(y => y.EmployeeId);

            Console.WriteLine("Ship Country" + "  " + "Customer ID" + "  " + "Order ID" + "  " + "Employee ID");

            foreach (var item in query)
            {
                Console.WriteLine(item.ShipCountry + "            " + item.CustomerId + "       " + item.OrderId + "        " +  item.EmployeeId);
            }
        }
    }
}
