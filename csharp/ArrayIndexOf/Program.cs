﻿using System;
using ConsoleAppLibrary;

namespace ArrayIndexOf
{
    class Program
    {
        static void Main(string[] args)
        {
            string stateToFind;
            do
            {
                Console.Write("Please enter a state to find (or press Enter to quit) : ");
                stateToFind = Console.ReadLine();
                if (!string.IsNullOrEmpty(stateToFind))
                {
                    int pos = Array.IndexOf(State.States, stateToFind);
                    if (pos == -1)
                        Console.WriteLine("'{0}' wasn't found", stateToFind);
                    else
                        Console.WriteLine("'{0}' was found at postion {1}", stateToFind, Convert.ToString(pos));
                }
            } while (!String.IsNullOrEmpty(stateToFind));
        }
    }
}
