﻿using ConsoleAppLibrary;
using System;
using System.Collections.Generic;

namespace CorrelatedSubQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            Sql sqlDemo = new Sql();
            List<Orders> orders = sqlDemo.GetOrdersWithPriceUnder3Dollars();

            foreach(var order in orders)
            {
                Console.WriteLine("{0} {1}", order.ID.ToString(), order.OrderDate.ToString());
            }
        }
    }
}
