﻿using System;
using ConsoleAppLibrary;
using ConsoleAppLibrary.Functions;

namespace GetStatisticByProduct
{
    class Program
    {
        static void Main(string[] args)
        {
            Function func = new Function();
            Statistic stats = func.GetStatisticByProduct(3);

            Console.WriteLine("Number item : {0:G}", stats.numberItems);
            Console.WriteLine("Unit Sold : {0:G}", stats.unitySold);
            Console.WriteLine("Total Sales : {0,2:C}", stats.totalSales);
        }
    }
}
