﻿using System;
using ConsoleAppLibrary.StoredProcedures;

namespace InsertShipper
{
    class Program
    {
        static void Main(string[] args)
        {
            string companyName = string.Empty;
            char exit = 'N';
            do
            {
                Console.Write("Enter company name : "); 
                companyName = Console.ReadLine();
                if (!string.IsNullOrEmpty(companyName))
                {
                    Console.Write("Enter phone number : ");
                    string phone = Console.ReadLine();
                    StoredProcedure sp = new StoredProcedure();
                    int lastShipperID = sp.InnsertShipper(companyName, phone);
                    if(lastShipperID > 0)
                    {
                        Console.Write("insert success : " + lastShipperID + Environment.NewLine);
                    }
                    else
                    {
                        Console.Write("insert fail\n");
                    }

                    Console.Write("To continue, press (Y/N) ");
                    exit = Convert.ToChar(Console.ReadLine());
                }  
            } while (exit != 'Y' || string.IsNullOrEmpty(companyName));
        }
    }
}
