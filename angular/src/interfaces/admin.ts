/*
* Program : Interface ICategorie
* Écrit par : Dan Duc Dao
*/

export interface IAdmin
{
    _id:string;
    employee:object;
    username:string;
    password:string;
    dateCreation:string;
    active:boolean;
}
