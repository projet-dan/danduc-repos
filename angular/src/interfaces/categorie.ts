/*
* Program : Interface ICategorie
* Écrit par : Dan Duc Dao
*/

export interface ICategorie {
  id: number;
  nom: string;
  description: string;
  photo: string;
  active: number;
}
