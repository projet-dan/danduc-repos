/*
* Program : Interface IEmployee
* Écrit par : Dan Duc Dao
*/

export interface IEmployee{
    _id:string,
    name:string,
    age:number
}
