/*
* Program : Set up express
* Écrit par : Dan Duc Dao
*/

const express = require("express");
const mysql = require("mysql");
const bodyParse = require("body-parser");
const cors = require("cors");
const app = express();

const acteurController = require("./controllers/movies/acteurController");
const adminController = require("./controllers/foods/adminController");
const emailController = require("./controllers/foods/emailController");
const employeeController = require("./controllers/foods/employeeController");
const filmController = require("./controllers/movies/filmController");
const foodCategorieController = require("./controllers/foods/categorieController");
const fournisseurController = require("./controllers/foods/fournisseurController");
const googleMapController = require("./controllers/googleMapController");
const movieCategorieController = require("./controllers/movies/categorieController");
const produitController = require("./controllers/foods/produitController");
const shoppingCartController = require("./controllers/foods/shoppingCartController");
const shoppingCartMusicController = require("./controllers/musics/shoppingCartController");

// connection bd food
const food = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "asiavn73",
  database: "food"
});

// connection bd music
const music = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "asiavn73",
  database: "music"
});

// connection bd movie
const movie = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "asiavn73",
  database: "movie"
});

// connect to database
food.connect();
music.connect();
movie.connect();

//app.use(bodyParse.json());
app.use(bodyParse({ limit: "50mb" }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//Acteur controller
acteurController(app, movie);

//Admin controller
adminController(app, food);

//Email controller
emailController(app);

//Employee controller
employeeController(app, food);

//film controller
filmController(app, movie);

//Categorie controller
foodCategorieController(app, food);

//Fournisseur controller
fournisseurController(app, food);

//Google map controller
googleMapController(app);

//categorie controller
movieCategorieController(app, movie);

//Produit controller
produitController(app, food);

//Shopping cart food controller
shoppingCartController(app, food);

//shopping cart music controller
shoppingCartMusicController(app, music);

app.use(function(err, req, res, next) {
  res.status(422).send({ erreur: err.message });
});

app.use(
  cors({
    origin: "*",
    credentials: true,
    exposedHeaders: ["Content-Type"]
  })
);

app.listen(4000, function() {
  console.log("connection avec succès");
});
