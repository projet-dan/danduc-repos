"use strict";

const { OK, BAB_REQUEST } = require('../../config');
const moment = require("moment");

module.exports = function(app, food) {
  app.get("/produit", function(req, res, next) {
    food.query("SELECT fournisseurs.nom AS fournisseur_nom, " +
                "categories.nom AS categorie_nom, " +
                "produits.* " +
                "FROM produits LEFT JOIN categories " +
                "ON produits.categorie_id = categories.id " +
                "LEFT JOIN fournisseurs " +
                "ON produits.fournisseur_id = fournisseurs.id " +
                "ORDER BY produits.nom",
              1,
              function(error, results) 
              {
                  if (error) 
                  {
                      console.error(`${error.code}\n${error.sqlMessage}`);
                      return res.status(BAB_REQUEST).send();
                  }
                  return res.status(OK).send(results);
              });
  });

  app.get("/produit/:id", function(req, res, next) {
    const productId = req.params.id;
    if (!productId || productId === undefined)
       return res.status(BAB_REQUEST).send();
    food.query("SELECT * FROM produits WHERE id = ? ORDER BY nom", productId, function(error,results) 
    {
      if (error) 
      {
         console.error(`${error.code}\n${error.sqlMessage}`);
         return res.status(BAB_REQUEST).send();
      }
      return res.status(OK).send(results[0]);
    });
  });

  app.post("/produit", function(req, res, next) {
    const {
            nom,
            fournisseur_id,
            categorie_id,
            quantite_par_unite,
            prix,
            quantite_en_stock,
            quantite_commande,
            reapprovisionnement
           } = req.body;
    food.query("INSERT produits(nom," +
                "fournisseur_id," +
                "categorie_id," +
                "quantite_par_unite," +
                "prix," +
                "quantite_en_stock," +
                "quantite_commande," +
                "reapprovisionnement," +
                "created_at) " +
                "VALUES(?,?,?,?,?,?,?,?,?)",
              [
                nom,
                fournisseur_id,
                categorie_id,
                quantite_par_unite,
                (prix = prix === "" ? null : produit.prix),
                (quantite_en_stock = quantite_en_stock === "" ? null : quantite_en_stock),
                (quantite_commande = quantite_commande === "" ? null : quantite_commande),
                (reapprovisionnement = reapprovisionnement === ""? null : reapprovisionnement),
                moment().format("YYYY-MM-DD hh:mm:ss")
              ],
              function(error, results) 
              {
                if (error || !results || results.insertId === 0)
                {
                    if(error)
                      console.error(`${error.code}\n${error.sqlMessage}`);
                    else
                      console.error(`row(s) inserted : ${results.insertId}`);
                    return res.status(BAB_REQUEST).send();
                }
                return res.status(OK).send({ success: true });
              }
            );
  });

  app.put("/produit", function(req, res, next) {
    const {
            nom, 
            fournisseur_id,
            categorie_id,
            quantite_par_unite,
            prix,
            quantite_en_stock,
            quantite_commande,
            reapprovisionnement,
            discontinue,
            active 
          } = req.body;
    food.query("UPDATE produits SET nom = ?," +
                "fournisseur_id = ?," +
                "categorie_id = ?," +
                "quantite_par_unite = ?," +
                "prix = ?," +
                "quantite_en_stock = ?," +
                "quantite_commande = ?," +
                "reapprovisionnement = ?," +
                "discontinue = ?," +
                "active = ?," +
                "updated_at = ? " +
                "WHERE id = ?",
              [
                nom,
                fournisseur_id,
                categorie_id,
                quantite_par_unite,
                (prix = prix === "" ? null : prix),
                (quantite_en_stock = quantite_en_stock === "" ? null : quantite_en_stock),
                (quantite_commande = quantite_commande === "" ? null : quantite_commande),
                (reapprovisionnement = reapprovisionnement === ""? null : reapprovisionnement),
                discontinue,
                active,
                moment().format("YYYY-MM-DD hh:mm:ss"),
                produit.id
              ],
              function(error, result) {
                if (error || !result || result.affectedRows === 0) 
                {
                    if(error)
                      console.error(`${error.code}\n${error.sqlMessage}`);
                    else
                      console.error(`row(s) updated : ${result.affectedRows}`);
                    return res.status(BAB_REQUEST).send();
                }
                return res.status(OK).send({ success: true });
              });
  });

  app.delete("/produit/:id", function(req, res, next) {
    const productId = req.params.id;
    if (!productId || productId === undefined)
       return res.status(BAB_REQUEST).send();
    food.query("UPDATE produits SET active = ? WHERE id = ?", [0, productId], function(error,result)
    {
      if (error)
      {
          if(error)
            console.error(`${error.code}\n${error.sqlMessage}`);
          else
            console.error(`row(s) updated : ${result.affectedRows}`);
      }
      return res.status(OK).send({ success: true });
    });
  });
};
