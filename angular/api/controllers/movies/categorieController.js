"use strict";

const { OK, BAB_REQUEST } = require('../../config');

module.exports = function(app, movie) {
  app.put("/movieadmin/categorie/:id", function(req, res, next) {
    movie.query("UPDATE categories " +
                "INNER JOIN categorie_films ON categories.id = categorie_films.categorie_id " +
                "INNER JOIN films ON categorie_films.film_id = films.id " +
                "SET categories.active = ?, categorie_films.active = ?, films.active = ? " +
                "WHERE categories.id = ?",
                [0, 0, 0, req.params.id],
                function(error, result) {
                  if (error || !result || result.affectedRows === 0)
                  {
                      if(error)
                        console.error(`${error.code}\n${error.sqlMessage}`);
                      else
                        console.error(`row(s) updated : ${result.affectedRows}`);
                      return res.status(BAB_REQUEST).send();
                  }
                  return res.status(OK).send({ success: true });
                });
    });
};
