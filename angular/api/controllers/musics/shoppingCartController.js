"use strict";

const { OK, BAB_REQUEST } = require('../../config');

module.exports = function(app, music) {
  app.get("/shoppingCartMusic/genre", function(req, res, next) {
    music.query("SELECT * FROM genres WHERE active = ? ORDER BY nom",
                1,
                function(error, results) {
                  if (error) 
                    return res.status(BAB_REQUEST).send();
                  return res.status(OK).send(results);
                });
  });

  app.get("/shoppingCartMusic/album", function(req, res, next) {
    music.query("SELECT * FROM albums WHERE active = ? ORDER BY titre",
                1,
                function(error, results) 
                {
                  if (error) 
                     return res.status(BAB_REQUEST).send();
                  return res.status(OK).send(results);
                });
  });

  app.get("/shoppingCartMusic/album/:id", function(req, res, next) {
    const albumId = req.params.id;
    if (!albumId || albumId === undefined) 
      return res.status(BAB_REQUEST).send();
    music.query("SELECT artistes.nom_complet AS artiste_nom, " +
                "genres.id AS genre_id, " +
                "genres.nom AS genre_nom, " +
                "albums.* " +
                "FROM albums " +
                "INNER JOIN artistes " +
                "ON artistes.id = albums.artiste_id " +
                "INNER JOIN genres " +
                "ON genres.id = albums.genre_id " +
                "WHERE albums.id = ? AND albums.active = ?",
                [albumId, 1],
                function(error, result) 
                {
                  if (error)
                    return res.status(BAB_REQUEST).send();
                  return res.status(OK).send(result);
                });
  });
};
