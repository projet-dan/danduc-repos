window.Vue = require("vue");

export const bus = new Vue();

Vue.component(
  "autocomplete-component",
  require("./components/autocompleteComponent.vue")
);

Vue.component(
  "listing-component",
  require("./components/listingComponent.vue")
);

Vue.component(
  "textarea-component",
  require("./components/textareaComponent.vue")
);

Vue.component("radio-component", require("./components/radioComponent.vue"));

Vue.component(
  "fileupload-component",
  require("./components/fileUploadComponent.vue")
);

Vue.component("image-component", require("./components/imageComponent.vue"));

Vue.component("select-component", require("./components/selectComponent.vue"));

Vue.component(
  "select-multiple-component",
  require("./components/selectMultipleComponent.vue")
);

Vue.component(
  "submit-button-component",
  require("./components/submitButtonComponent.vue")
);

Vue.component(
  "back-button-component",
  require("./components/backButtonComponent.vue")
);

const app = new Vue({
  el: "#wrapper"
});
