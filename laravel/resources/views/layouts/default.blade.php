<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="{{mix('bundle/css/all.css')}}"/>
</head>
<body>
    @include('partials.header')
    @include('partials.aside')
    <div id="wrapper">
        @include('partials.errors')
        @include('partials.success')
        @yield('content')  
    </div>
    <script src="{{mix('bundle/js/all.js')}}" ></script> 
</body>
</html>