@extends('layouts/default')
@section('content')
    <section id="hero" data-type="background" data-speed="100">
      <article>
        <div class="container clearfix">
          <div class="row">
            <div class="col-sm-5">
              <img src="images/logo-badge.png" alt="Boostrap To WordPress" class="logo">
            </div>
            <div class="col-sm-7 hero-text">
              <h1>What is Lorem Ipsum</h1>
              <p class="lead">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
              </p>
              <div id="price-timeline">
                <div class="price active">
                  <h4>
                    Pre-Launch Price
                    <small>Ends soon!</small>
                  </h4>
                  <span>$149</span>
                </div>
                <div class="price">
                  <h4>
                    Launch Price
                    <small>Coming soon!</small>
                  </h4>
                  <span>$299</span>
                </div>
                <div class="price">
                  <h4>
                    Final Price
                    <small>Coming soon!</small>
                  </h4>
                  <span>$399</span>
                </div>
              </div>
              <p>
                <a class="btn btn-lg btn-danger" href="/" role="button">Enroll &raquo</a>
              </p>
            </div>
          </div>
        </div>
      </article>
    </section>
    <section id="optin">
      <div class="container">
        <div class="row">
          <div class="col-sm-8">
            <p class="lead">
              <strong>Subscribe to our mailing list.</strong> We'll send something special as a thank you
            </p>
          </div>
          <div class="col-sm-4">
            <button
              class="btn btn-success btn-lg btn-block"
              data-toggle="modal"
              data-target="#myModal"
            >Click here to subscribe</button>
          </div>
        </div>
      </div>
    </section>
    <section id="boost-income">
      <div class="container">
        <div class="section-header">
          <img :src="'images/icon-boost.png'" alt="Chart">
          <h2>What is Lorem Ipsum</h2>
        </div>
        <p class="lead">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
          industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
          and scrambled it to make a type specimen book.
        </p>
        <div class="row">
          <div class="col-sm-6">
            <h3>Where does it come from</h3>
            <p>
              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
              Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced
              in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
            </p>
          </div>
          <div class="col-sm-6">
            <h3>Where can I get some</h3>
            <p>
              There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration
              in some form, by injected humour, or randomised words which don't look even slightly believable. If you
              are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden
              in the middle of text.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section id="who-benifits">
      <div class="container">
        <div class="section-header">
          <img src="images/icon-pad.png" alt="Pad and Pencil">
          <h2>Where does it come from</h2>
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <h3>What is Lorem Ipsum</h3>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
              <h3>What is Lorem Ipsum</h3>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
              <h3>What is Lorem Ipsum</h3>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="course-features">
      <div class="container">
        <div class="section-header">
          <img :src="'images/icon-rocket.png'">
          <h2>What is Lorem Ipsum</h2>
        </div>
        <div class="row">
          <div class="col-sm-2">
            <i class="ci ci-computer"></i>
            <h4>Where does it come from</h4>
          </div>
          <div class="col-sm-2">
            <i class="ci ci-watch"></i>
            <h4>Where does it come from</h4>
          </div>
          <div class="col-sm-2">
            <i class="ci ci-calendar"></i>
            <h4>Where does it come from</h4>
          </div>
          <div class="col-sm-2">
            <i class="ci ci-community"></i>
            <h4>Where does it come from</h4>
          </div>
          <div class="col-sm-2">
            <i class="ci ci-instructor"></i>
            <h4>Where does it come from</h4>
          </div>
          <div class="col-sm-2">
            <i class="ci ci-device"></i>
            <h4>Where does it come from</h4>
          </div>
        </div>
      </div>
    </section>
    <section id="project-features">
      <div class="container">
        <h2>What is Lorem Ipsum</h2>
        <p class="lead">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          Lorem Ipsum has been the industry's standard dummy text ever since the
          1500s, when an unknown printer took a galley of type and scrambled it
          to make a type specimen book.
        </p>
        <div class="row">
          <div class="col-sm-4">
            <img :src="'images/icon-design.png'" alt="Design">
            <h3>What is Lorem Ipsum</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled it
              to make a type specimen book.
            </p>
          </div>
          <div class="col-sm-4">
            <img :src="'images/icon-code.png'" alt="Code">
            <h3>What is Lorem Ipsum</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled it
              to make a type specimen book.
            </p>
          </div>
          <div class="col-sm-4">
            <img :src="'images/icon-cms.png'" alt="CMS">
            <h3>What is Lorem Ipsum</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled it
              to make a type specimen book.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section id="featurette">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <h2>Welcom to Udemy's Youtube Channel</h2>
            <iframe
              width="100%"
              height="415"
              src="https://www.youtube.com/embed/vMqAXTEgEM0"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </div>
        </div>
      </div>
    </section>
    <section id="instructor">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-6">
            <div class="row">
              <div class="col-lg-8">
                <h2>What is Lorem Ipsum</h2>
              </div>
              <div class="col-lg-4">
                <a href="#" class="badge social twitter">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="#" class="badge social facebook">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="#" class="badge social gplus">
                  <i class="fa fa-google-plus"></i>
                </a>
              </div>
            </div>
            <p class="lead">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a type
              specimen book.
            </p>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a type
              specimen book.
            </p>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a type
              specimen book.
            </p>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a type
              specimen book.
            </p>
            <hr>
            <div class="row">
              <div class="col-xs-4">
                <div class="num">
                  <div class="num-content">
                    41,000+
                    <span>students</span>
                  </div>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="num">
                  <div class="num-content">
                    568
                    <span>reviews</span>
                  </div>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="num">
                  <div class="num-content">
                    8
                    <span>courses</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="kudos">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <h2>Where does it come from</h2>
            <div class="row testimonial">
              <div class="col-sm-4">
                <img :src="'images/brennan.jpg'" alt="Brennan">
              </div>
              <div class="col-sm-8">
                <blockquote>
                  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                  those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                  by Cicero are also reproduced in their exact original form, accompanied by English
                  versions from the 1914 translation by H. Rackham.
                  <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                </blockquote>
              </div>
            </div>
            <div class="row testimonial">
              <div class="col-sm-4">
                <img :src="'images/ben.png'" alt="Ben">
              </div>
              <div class="col-sm-8">
                <blockquote>
                  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                  those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                  by Cicero are also reproduced in their exact original form, accompanied by English
                  versions from the 1914 translation by H. Rackham.
                  <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                </blockquote>
              </div>
            </div>
            <div class="row testimonial">
              <div class="col-sm-4">
                <img :src="'images/aj.png'" alt="AJ">
              </div>
              <div class="col-sm-8">
                <blockquote>
                  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                  those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                  by Cicero are also reproduced in their exact original form, accompanied by English
                  versions from the 1914 translation by H. Rackham.
                  <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                </blockquote>
              </div>
            </div>
            <div class="row testimonial">
              <div class="col-sm-4">
                <img :src="'images/ernest.png'" alt="Ernest">
              </div>
              <div class="col-sm-8">
                <blockquote>
                  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                  those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                  by Cicero are also reproduced in their exact original form, accompanied by English
                  versions from the 1914 translation by H. Rackham.
                  <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="signup" data-type="background" data-speed="4">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3">
            <h2>What is Lorem Ipsum</h2>
            <p>
              <a href="#" class="btn btn-lg btn-block btn-success">Yes sign me up!</a>
            </p>
          </div>
        </div>
      </div>
    </section>

    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="myModelLabel">
              <i class="fa fa-envelope">Subscribe to our mailing list</i>
            </h4>
          </div>
          <div class="modal-body">
            <p>
              It is a long established fact that a reader will be distracted by the readable content of a page when
              looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
              of letters, as opposed to using 'Content here, content here', making it look like
              <em>readable English</em>.
            </p>
            <form class="form-inline" role="form">
              <div class="form-group">
                <label class="sr-only" for="subscribe-name">Your first name</label>
                <input
                  type="text"
                  class="form-control"
                  id="subscribe-name"
                  placeholder="Your forst name"
                >
              </div>
              <div class="form-group">
                <label class="sr-only" for="subscribe-email">and your email</label>
                <input
                  type="text"
                  class="form-control"
                  id="subscribe-email"
                  placeholder="and your email"
                >
              </div>
              <input type="submit" class="btn btn-danger" value="Subscribe">
            </form>
            <hr>
            <p>
              <small>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                the industry's standard dummy text ever since the 1500s,
                <br>when an unknown printer took a galley of type and
                scrambled it to make a type specimen book
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>
@endsection
