 <!-- Header -->
 <div id="header">
        <div class="color-line">
        </div>
        <div id="logo" class="light-version">
            <span>
                Exemple - Laravel
            </span>
        </div>
        <nav role="navigation">
            <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
            <div class="small-logo">
                <span class="text-primary">Exemple - Laravel</span>
            </div>
            <div class="navbar-right">
                <ul class="nav navbar-nav no-borders">
                    @guest
                        <li class="dropdown">
                            <a class="nav-link" href="{{ route('login') }}">
                                <i class="pe-7s-lock"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="nav-link" href="{{ route('register') }}">
                                <i class="pe-7s-add-user"></i>
                            </a>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" style="font-size:17px;">{{ Auth::user()->name }}</a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-item" 
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                    <i class="pe-7s-unlock"></i>
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest 
                </ul>
            </div>
        </nav>
    </div>