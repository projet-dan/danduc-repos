<aside id="menu" >
    <div id="navigation">
        <div class="profile-picture">
            <a href="index.html">
                <img src={{URL::asset('images/logo.jpg')}} class="img-circle m-b" alt="logo" width="150" height="100" />
            </a>
        </div>
        <ul class="nav" id="side-menu">
            <li>
            <a href="/"><span class="nav-label">Home</span></a>
            </li>
            <li>
                <a href="#"><span class="nav-label">Movie admin</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('movieadmin/film')}}">Films</a></li>
                    <li><a href="{{url('movieadmin/categorie')}}">Catégories</a></li>
                    <li><a href="{{url('movieadmin/acteur')}}">Acteurs</a></li>	
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Movie store</span><span class="fa arrow"></span> </a>
                @php
                    $categories = App\Models\Movies\Categorie::all();
                @endphp
                <ul class="nav nav-second-level">
                    @foreach($categories as $categorie)
                        <li><a href="{{url('/moviestore/categorie/'.$categorie->id)}}">{{$categorie->nom}}</a></li>
                    @endforeach	
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Excel</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('/excel/write')}}">Écriture excel</a></li>
                    <li><a href="{{url('/excel/readXLSX')}}">Lecture excel (xslx)</a></li>
                    <li><a href="{{url('/excel/readCSV')}}">Lecture excel (csv)</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>