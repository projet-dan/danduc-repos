<?php

use Illuminate\Database\Seeder;

class MovieDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Movie store
        $this->call(PaysTableSeeder::class);
        $this->call(VillesTableSeeder::class); 
        $this->call(LanguesTableSeeder::class);
        $this->call(LangueOriginalsTableSeeder::class);
        $this->call(ActeursTableSeeder::class); 
        $this->call(AdressesTableSeeder::class); 
        $this->call(StoresTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(FilmsTableSeeder::class);
        $this->call(FilmTextesTableSeeder::class);
        $this->call(InventairesTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(CategorieFilmsTableSeeder::class);
        $this->call(ActeurFilmsTableSeeder::class);
    }
}
