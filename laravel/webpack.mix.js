let mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .sass("resources/assets/sass/app.scss", "public/bundle/css/app.css")
  .sass(
    "resources/assets/sass/autocomplete.scss",
    "public/bundle/css/autocomplete.css"
  )
  .styles([
            "public/css/vendor/fontawesome/css/font-awesome.css",
            "public/css/vendor/metisMenu/dist/metisMenu.css",
            "public/css/vendor/animate.css/animate.css",
            "public/css/vendor/bootstrap/dist/css/bootstrap.css",
            "public/css/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css",
            "public/css/fonts/pe-icon-7-stroke/css/helper.css",
            "public/css/styles/style.css",
            "public/bundle/css/app.css",
            "public/bundle/css/autocomplete.css"
          ], 'public/bundle/css/all.css')
  .js('resources/assets/js/app.js', 'public/bundle/js/app.js')
  .babel('public/js/app.js', 'public/bundle/js/app.es5.js')
  .scripts([
              'public/bundle/js/app.es5.js',
              'public/js/vendor/jquery/dist/jquery.min.js',
              'public/js/vendor/jquery-ui/jquery-ui.min.js',
              'public/js/vendor/slimScroll/jquery.slimscroll.min.js',
              'public/js/vendor/bootstrap/dist/js/bootstrap.min.js',
              'public/js/vendor/metisMenu/dist/metisMenu.min.js',
              'public/js/vendor/iCheck/icheck.min.js',
              'public/js/vendor/sparkline/index.js',
              'public/js/vendor/homer.js'     
            ],'public/bundle/js/all.js')
  .options({
    processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
  })