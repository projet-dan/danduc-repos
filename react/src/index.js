import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

import "./css/vendor/fontawesome/css/font-awesome.css";
import "./css/vendor/metisMenu/dist/metisMenu.css";
import "./css/vendor/animate.css/animate.css";
import "./css/vendor/bootstrap/dist/css/bootstrap.css";
import "./css/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css";
import "./css/fonts/pe-icon-7-stroke/css/helper.css";
import "./css/styles/style.css";
import "./scss/main.scss";

ReactDOM.render(<Provider store= { createStore(reducers, applyMiddleware(thunk)) }>
                     <App />
                </Provider>,
                document.getElementById("root"));
                 


