/*
* Program : Classe Service 
* Écrit par : Dan Duc Dao
*/

import axios from "axios";

export class Service {
  constructor(protocol,domain,path) {
    this.protocol = "http://";
    this.domain = "localhost:4000";
    this.path = "";
    this.authorization="";
  }

  setProtocol = (protocol) => {
    this.protocol = protocol;
  }
  getProtocol = () => {
    return this.protocol;
  }
  setDomain = (domain) => {
    this.domain = domain
  }
  getDomain = () => {
    return this.domain;
  }
  setAuthorization = (authorization) => {
    this.authorization = authorization;
  }
  getAuthorization = () => {
     return this.authorization;
  }
  get(path) {
    if (typeof path === undefined || path === "") return;
    if(this.getAuthorization())
    {
      return axios.get(this.getProtocol() + this.getDomain() + path,{
          headers : { Authorization : this.getAuthorization() }
      });
    }else{
      return axios.get(this.getProtocol() + this.getDomain() + path);
    }
  }
}
