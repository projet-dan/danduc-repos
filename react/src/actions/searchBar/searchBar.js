export const selectedItem = (currentItem,selectedItem) => {
    return {
        type:'ITEM_SELECTED',
        payload:{ currentItem , selectedItem }
    }
}

export const doSearchInList = (term,currentItemState, collections) => {
    return {
        type:'DO_SEARCH_LIST',
        payload:{ term, currentItemState, collections}
    }
}