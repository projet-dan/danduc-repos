import service from '../../api/unplashAPI';

export const fetchCollections = () => async dispatch => {
    const response = await service.get('/collections');
    dispatch({
        type:'FETCH_COLLECTIONS',
        payload:response.data
    });
};