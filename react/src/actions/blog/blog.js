import service from '../../api/jsonPlaceHolderAPI';

export const fetchPosts = () => async dispatch => {
    const response = await service.get('/posts');
    dispatch({
        type:'FETCH_POSTS',
        payload:response.data
    });
};

export const fetchUsers = () => async dispatch => {
    const response = await service.get('/users');
    dispatch({
        type:'FETCH_USERS',
        payload:response.data
    });
};



