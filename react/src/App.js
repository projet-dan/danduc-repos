import React, { Component } from "react";
import Home from "./components/home";
import HelloWorld from "./components/react/helloWorld";
import InlineStyle from "./components/react/inlineStyle";
import GetTime from "./components/react/getTime";
import Blog from "./components/react/blog";
import Season from "./components/react/season";
import Songs from "./components/react/song";
import SearchBar from "./components/react/searchBar";
import MusicStore from "./components/musicstore";
import Checkout from "./components/musicstore/checkout";
import Album from "./components/musicstore/album";
import Header from "./components/partials/header";
import Aside from "./components/partials/aside";
import { Service } from "./services/service";
import { Route, Switch, HashRouter } from "react-router-dom";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
                    styleObject: {
                                    fontWeight: "bold",
                                    backgroundColor: "#32454D",
                                    color: "#ffffff"
                                    },
                    erreur: "",
                    genres: [],
                    service: new Service()
                 }
  }
  componentDidMount() {
    this.loadData();
  }
  loadData = () => {
    this.state.service
      .get("/shoppingCartMusic/genre")
      .then(result => {
        let genres = result.data;
        if (Object.keys(genres).length > 0) {
          genres = [...this.state.genres];
          genres = result.data;
          this.setState(prevState => {
              return {
                        erreur: false,
                        genres: genres
                     }
          });
        }
      })
      .catch(error => {
        console.error("erreur: ", error);
        this.setState(prevState => {
           return { erreur: `${error}` }
        });
      });
  };

  render() {
    const { erreur, genres } = this.state;
    if (erreur) {
      return (
        <div>
          Une erreur s'est produite lors du chargement des données. Veuillez vérifier la connection API
          <button onClick={this.loadData}>Veuillez réessayer</button>
        </div>
      );
    }
    return (
      <HashRouter>
        <React.Fragment>
          <Header />
          <Aside categories={ genres } 
                 activeStyle = { this.state.styleObject } />
          <div id="wrapper">
                <Switch>
                  <Route exact path="/" component={ Home } />
                  <Route exact path="/helloWorld" component={ HelloWorld } />
                  <Route exact path="/inlineStyle" component={ InlineStyle } />
                  <Route exact path="/getTime" component={ GetTime } />
                  <Route exact path="/blog" component={ Blog } />
                  <Route exact path="/displaySeason" component={ Season } />
                  <Route exact path="/songList" component={ Songs } />
                  <Route exact path="/searchBar" component={ SearchBar } />
                  <Route exact path="/album/:id" component={ Album } />
                  <Route exact path="/genre/:id" component={ MusicStore } />
                  <Route exact path="/checkout" component={ Checkout } />
                </Switch>
          </div>
        </React.Fragment>
      </HashRouter>
    );
  }
}
