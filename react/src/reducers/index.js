import { combineReducers } from 'redux';
import { songsReducer,selectedSongReducer } from './song/songReducer';
import { postReducer } from './blog/postReducer';
import { userReducer } from './blog/userReducer';
import collectionReducer from './searchBar/collectionReducer';
import { initItemReducer, 
         currentItemStateReducer, 
         doSearchInListReducer } from './searchBar/searchBarReducer';

export default combineReducers({
    songs : songsReducer,
    selectedSong : selectedSongReducer,
    posts : postReducer,
    users : userReducer,
    initItem : initItemReducer,
    currentItemState : currentItemStateReducer,
    currentItemSelected : doSearchInListReducer,
    collections : collectionReducer
});

