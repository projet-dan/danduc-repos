
export const initItemReducer = () => {
    return [
        { id:1, clsAttribut:'icheckbox_square-green', checked:false,name:'title',text:'Title',value:'title' },
        { id:2, clsAttribut:'icheckbox_square-green', checked:false, name:'description',text:'Description',value:'description' },
    ];
};

export const currentItemStateReducer = (item = null, action) => {
    if(action.type === 'ITEM_SELECTED')
    {
        let { clsAttribut, checked, name} = action.payload.selectedItem;
        return action.payload.currentItem.reduce((newItem,oldItem) => {      
            if(oldItem.name === name)
            {
                oldItem.checked = checked === true? false : true;
                oldItem.clsAttribut = checked === true? clsAttribut.split(" ")[0]:clsAttribut + " checked";
            }
            newItem.push(oldItem)
            return newItem;
        },[]);
    }
    return item;
}

export const doSearchInListReducer = (item=[],action) =>{
    if(action.type === 'DO_SEARCH_LIST')
    {
        const { currentItemState, collections, term } = { ...action.payload };
        let results = [];
        if(term)
        {
            currentItemState.forEach(element => {
                if(element.checked === true)
                {
                    results = results.concat(collections.filter(collection => collection[element.value] != null && collection[element.value].toLowerCase().indexOf(term.toLowerCase()) >= 0));
                }
            });
        }
        return results;
    }
    return item;
}
