const collectionReducer = (item=[],action) => {
    if(action.type === 'FETCH_COLLECTIONS')
    {
        return action.payload;
    }
    return item;
}

export default collectionReducer;