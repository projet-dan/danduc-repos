import React from "react";

const Spinner = (props) => {
    return (
        <div><i className={ `fa fa-spinner` } />&nbsp;<span style={{ color:"red" }}>{ props.message }</span></div>
    )
}

Spinner.defaultProps = {
    message: 'Loading..'
}
export default Spinner;