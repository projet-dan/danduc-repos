/*
* Program : HomeComponent
* Écrit par : Dan Duc Dao
*/

import React from "react";
import { NavLink } from "react-router-dom";

const Home = () =>{
   return (
            <React.Fragment>
                <section id="hero" data-type="background" data-speed="100">
                  <article>
                    <div className="container clearfix">
                      <div className="row">
                        <div className="col-sm-5">
                          <img src="./assets/images/logo-badge.png" alt="Boostrap To WordPress" className="logo" />
                        </div>
                        <div className="col-sm-7 hero-text">
                          <h1>What is Lorem Ipsum</h1>
                          <p className="lead">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                          </p>
                          <div id="price-timeline">
                            <div className="price active">
                              <h4>
                                Pre-Launch Price
                                <small>Ends soon!</small>
                              </h4>
                              <span>$149</span>
                            </div>
                            <div className="price">
                              <h4>
                                Launch Price
                                <small>Coming soon!</small>
                              </h4>
                              <span>$299</span>
                            </div>
                            <div className="price">
                              <h4>
                                Final Price
                                <small>Coming soon!</small>
                              </h4>
                              <span>$399</span>
                            </div>
                          </div>
                          <p>
                            <NavLink className="btn btn-lg btn-danger" to="/" role="button">Enroll &raquo;</NavLink>
                          </p>
                        </div>
                      </div>
                    </div>
                  </article>
                </section>
                <section id="optin">
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-8">
                        <p className="lead">
                          <strong>Subscribe to our mailing list.</strong> We'll send something special as a thank you
                        </p>
                      </div>
                      <div className="col-sm-4">
                        <button
                          className="btn btn-success btn-lg btn-block"
                          data-toggle="modal"
                          data-target="#myModal"
                        >Click here to subscribe</button>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="boost-income">
                  <div className="container">
                    <div className="section-header">
                      <img src="./assets/images/icon-boost.png" alt="Chart" />
                      <h2>What is Lorem Ipsum</h2>
                    </div>
                    <p className="lead">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                      industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                      and scrambled it to make a type specimen book.
                    </p>
                    <div className="row">
                      <div className="col-sm-6">
                        <h3>Where does it come from</h3>
                        <p>
                          The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
                          Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced
                          in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                        </p>
                      </div>
                      <div className="col-sm-6">
                        <h3>Where can I get some</h3>
                        <p>
                          There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration
                          in some form, by injected humour, or randomised words which don't look even slightly believable. If you
                          are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden
                          in the middle of text.
                        </p>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="who-benifits">
                  <div className="container">
                    <div className="section-header">
                      <img src="./assets/images/icon-pad.png" alt="Pad and Pencil" />
                      <h2>Where does it come from</h2>
                      <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                          <h3>What is Lorem Ipsum</h3>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                            a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                          </p>
                          <h3>What is Lorem Ipsum</h3>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                            a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                          </p>
                          <h3>What is Lorem Ipsum</h3>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                            a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="course-features">
                  <div className="container">
                    <div className="section-header">
                      <img src="./assets/images/icon-rocket.png" alt="icon-rocket" />
                      <h2>What is Lorem Ipsum</h2>
                    </div>
                    <div className="row">
                      <div className="col-sm-2">
                        <i className="ci ci-computer"></i>
                        <h4>Where does it come from</h4>
                      </div>
                      <div className="col-sm-2">
                        <i className="ci ci-watch"></i>
                        <h4>Where does it come from</h4>
                      </div>
                      <div className="col-sm-2">
                        <i className="ci ci-calendar"></i>
                        <h4>Where does it come from</h4>
                      </div>
                      <div className="col-sm-2">
                        <i className="ci ci-community"></i>
                        <h4>Where does it come from</h4>
                      </div>
                      <div className="col-sm-2">
                        <i className="ci ci-instructor"></i>
                        <h4>Where does it come from</h4>
                      </div>
                      <div className="col-sm-2">
                        <i className="ci ci-device"></i>
                        <h4>Where does it come from</h4>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="project-features">
                  <div className="container">
                    <h2>What is Lorem Ipsum</h2>
                    <p className="lead">
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      Lorem Ipsum has been the industry's standard dummy text ever since the
                      1500s, when an unknown printer took a galley of type and scrambled it
                      to make a type specimen book.
                    </p>
                    <div className="row">
                      <div className="col-sm-4">
                        <img src="./assets/images/icon-design.png" alt="Design" />
                        <h3>What is Lorem Ipsum</h3>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the
                          1500s, when an unknown printer took a galley of type and scrambled it
                          to make a type specimen book.
                        </p>
                      </div>
                      <div className="col-sm-4">
                        <img src="./assets/images/icon-code.png" alt="Code" />
                        <h3>What is Lorem Ipsum</h3>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the
                          1500s, when an unknown printer took a galley of type and scrambled it
                          to make a type specimen book.
                        </p>
                      </div>
                      <div className="col-sm-4">
                        <img src="./assets/images/icon-cms.png" alt="CMS" />
                        <h3>What is Lorem Ipsum</h3>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the
                          1500s, when an unknown printer took a galley of type and scrambled it
                          to make a type specimen book.
                        </p>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="featurette">
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-8 col-sm-offset-2">
                        <h2>Welcom to Udemy's Youtube Channel</h2>
                        <iframe
                          title="Welcom to Udemy's Youtube Channel"
                          width="100%"
                          height="415"
                          src="https://www.youtube.com/embed/vMqAXTEgEM0"
                          frameborder="0"
                          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                          allowfullscreen
                        ></iframe>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="instructor">
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-8 col-md-6">
                        <div className="row">
                          <div className="col-lg-8">
                            <h2>What is Lorem Ipsum</h2>
                          </div>
                          <div className="col-lg-4">
                            <NavLink to="#" className="badge social twitter">
                              <i className="fa fa-twitter"></i>
                            </NavLink>
                            <NavLink to="#" className="badge social facebook">
                              <i className="fa fa-facebook"></i>
                            </NavLink>
                            <NavLink to="#" className="badge social gplus">
                              <i className="fa fa-google-plus"></i>
                            </NavLink>
                          </div>
                        </div>
                        <p className="lead">
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and scrambled it to make a type
                          specimen book.
                        </p>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and scrambled it to make a type
                          specimen book.
                        </p>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and scrambled it to make a type
                          specimen book.
                        </p>
                        <p>
                          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and scrambled it to make a type
                          specimen book.
                        </p>
                        <hr />
                        <div className="row">
                          <div className="col-xs-4">
                            <div className="num">
                              <div className="num-content">
                                41,000+
                                <span>students</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-xs-4">
                            <div className="num">
                              <div className="num-content">
                                568
                                <span>reviews</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-xs-4">
                            <div className="num">
                              <div className="num-content">
                                8
                                <span>courses</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="kudos">
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-8 col-sm-offset-2">
                        <h2>Where does it come from</h2>
                        <div className="row testimonial">
                          <div className="col-sm-4">
                            <img src="./assets/images/brennan.jpg" alt="Brennan" />
                          </div>
                          <div className="col-sm-8">
                            <blockquote>
                              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                              those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                              by Cicero are also reproduced in their exact original form, accompanied by English
                              versions from the 1914 translation by H. Rackham.
                              <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                            </blockquote>
                          </div>
                        </div>
                        <div className="row testimonial">
                          <div className="col-sm-4">
                            <img src="./assets/images/ben.png" alt="Ben" />
                          </div>
                          <div className="col-sm-8">
                            <blockquote>
                              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                              those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                              by Cicero are also reproduced in their exact original form, accompanied by English
                              versions from the 1914 translation by H. Rackham.
                              <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                            </blockquote>
                          </div>
                        </div>
                        <div className="row testimonial">
                          <div className="col-sm-4">
                            <img src="./assets/images/aj.png" alt="AJ" />
                          </div>
                          <div className="col-sm-8">
                            <blockquote>
                              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                              those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                              by Cicero are also reproduced in their exact original form, accompanied by English
                              versions from the 1914 translation by H. Rackham.
                              <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                            </blockquote>
                          </div>
                        </div>
                        <div className="row testimonial">
                          <div className="col-sm-4">
                            <img src="./assets/images/ernest.png" alt="Ernest" />
                          </div>
                          <div className="col-sm-8">
                            <blockquote>
                              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                              those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                              by Cicero are also reproduced in their exact original form, accompanied by English
                              versions from the 1914 translation by H. Rackham.
                              <cite>&mdash;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</cite>
                            </blockquote>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="signup" data-type="background" data-speed="4">
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-6 col-sm-offset-3">
                        <h2>What is Lorem Ipsum</h2>
                        <p>
                          <NavLink to="#" className="btn btn-lg btn-block btn-success">Yes sign me up!</NavLink>
                        </p>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                <div className="modal fade" id="myModal">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <button type="button" className="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span className="sr-only">Close</span>
                          </button>
                          <h4 className="modal-title" id="myModelLabel">
                            <i className="fa fa-envelope">Subscribe to our mailing list</i>
                          </h4>
                        </div>
                        <div className="modal-body">
                          <p>
                            It is a long established fact that a reader will be distracted by the readable content of a page when
                            looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                            of letters, as opposed to using 'Content here, content here', making it look like
                            <em>readable English</em>.
                          </p>
                          <form className="form-inline">
                            <div className="form-group">
                              <label className="sr-only" for="subscribe-name">Your first name</label>
                              <input
                                type="text"
                                className="form-control"
                                id="subscribe-name"
                                placeholder="Your forst name"
                              />
                            </div>
                            <div className="form-group">
                              <label className="sr-only" for="subscribe-email">and your email</label>
                              <input
                                type="text"
                                className="form-control"
                                id="subscribe-email"
                                placeholder="and your email"
                              />
                            </div>
                            <input type="submit" className="btn btn-danger" value="Subscribe" />
                          </form>
                          <hr />
                          <p>
                            <small>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                              the industry's standard dummy text ever since the 1500s,
                              <br />when an unknown printer took a galley of type and
                              scrambled it to make a type specimen book
                            </small>
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
                </section>
            </React.Fragment>
          );
}

export default Home;
