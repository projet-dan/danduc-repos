import React from "react";
import { NavLink } from "react-router-dom";

const Aside = (props) =>{
    return (
        <aside id="menu">
            <div id="navigation">
              <div className="profile-picture">
                <a href="index.html">
                  <img src="../assets/images/logo.jpg"
                       className="img-circle m-b"
                       alt="logo"
                       width="100"
                       height="100"/>
                </a>
              </div>
              <ul className="nav" id="side-menu">
                <li>
                    <NavLink to="/">Home</NavLink>
                </li>
                <li>
                  <NavLink to="#">
                      <span className="nav-label">React</span>
                      <span className="fa arrow" />
                  </NavLink>
                  <ul className="nav nav-second-level">
                      <li><NavLink to={"/helloWorld"} activeStyle={ props.activeStyle }>Hello World</NavLink></li>
                      <li><NavLink to={"/inlineStyle"}>Inline Style</NavLink></li>
                      <li><NavLink to={"/getTime"}>Get Time</NavLink></li>
                      <li><NavLink to={"/blog"}>Blog Post</NavLink></li>
                      <li><NavLink to={"/displaySeason"}>Display Season</NavLink></li>
                      <li><NavLink to={"/songList"}>Song List</NavLink></li>
                      <li><NavLink to={"/searchBar"}>Search Bar</NavLink></li>
                  </ul>
                </li>
                <li>
                  <NavLink to="#">
                    <span className="nav-label">Music store</span>
                    <span className="fa arrow" />
                  </NavLink>
                  <ul className="nav nav-second-level">
                    {props.categories.map((value, key) => (
                      <li key={key}>
                        <NavLink
                          to={"/genre/" + value.id}
                          replace
                          activeStyle={ props.activeStyle }
                        >
                          {value.nom}
                        </NavLink>
                      </li>
                    ))}
                  </ul>
                </li>
              </ul>
            </div>
          </aside>
    );
}

export default Aside;