import React from "react";

const Header = () =>{
     return (
        <div id="header">
            <div className="color-line" />
            <div id="logo" className="light-version">
            <span>React - Exemple</span>
            </div>
            <nav role="navigation">
                <div className="header-link hide-menu">
                    <i className="fa fa-bars" />
                </div>
                <div className="small-logo">
                    <span className="text-primary">React.js - Exemple</span>
                </div>
            </nav>
        </div>
     );
}

export default Header

