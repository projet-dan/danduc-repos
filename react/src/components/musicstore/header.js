/*
* Program : Header Component
* Écrit par : Dan Duc Dao
*/

import React from "react";
import { NavLink } from "react-router-dom";

const MSHeader = props => {
  let myCart = props.myCart.length > 0 ? (<NavLink to="#" className="showNumItem">
                                            {props.myCart.length} item(s)
                                          </NavLink>) : ("");
  return (
          <div className="hpanel" style={{ marginBottom:'5px'}}>
            <div className="panel-body">
              <div id="ms-header">
                <h1>{ props.title }</h1>
                <ul id="ms-navlist">
                  <li>
                    <NavLink to="#">Admin</NavLink>
                  </li>
                  <li>{myCart}</li>
                      {myCart ? (<li>
                                    <NavLink to="/checkout">{ props.checkoutVal }</NavLink>
                                </li>) : ("")}
                </ul>
              </div>
            </div>
          </div>
          );
};

export default MSHeader;
