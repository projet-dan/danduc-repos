/*
* Program : MusicStore component
* Écrit par : Dan Duc Dao
*/

import React, { Component } from "react";
import { Service } from "../../services/service";
import MSHeader from "./header";
import MSShowAlbum from "./showAlbum";
import { LocalStorage } from "../../classes/localstorage";
import { ShoppingCart } from "../../classes/shoppingCart";

export default class MusicStore extends Component {
  constructor() {
    super();
    this.service = new Service();
    this.localstorage = new LocalStorage();
    this.state = {
                  chargement: true,
                  erreur: "",
                  genres: null,
                  albums: null,
                  carts: []
                };
  }
  componentDidMount() {
    this.loadData();
    if (this.localstorage.itemExist("cart")) {
      this.setState(prevState => { 
                                    return { carts: this.localstorage.getItem("cart") }
                                  });
    }
  }
  loadData = () => {
    this.service.get("/shoppingCartMusic/genre")
                .then(result => {
                    if (Object.keys(result.data).length > 0) {
                      this.setState(prevState => {
                        return { genres: result.data }
                      });
                      return this.service.get("/shoppingCartMusic/album");
                    }
                }).then(result => {
                    let albums = result.data;
                    if (Object.keys(albums).length > 0) {
                      this.setState(prevState => {
                        return {
                                  chargement: false,
                                  erreur: false,
                                  albums: albums,
                                  albumsSearch: albums
                                }
                      });
                    }
                }).catch(error => {
                    console.error("erreur: ", error);
                    this.setState(prevState => {
                        return { erreur: `${error}`,
                                chargement: false
                                }
                    });
                });
  }
  AddItem = (albumId) => {
    let album = [];
    let carts = [];
    this.service.get("/shoppingCartMusic/album/" + albumId)
                .then(res => {
                    album = res.data;
                    if (album.length > 0) {
                      if (this.localstorage.itemExist("cart")) {
                        carts = this.localstorage.getItem("cart");
                        let index = carts.findIndex(res => res.id === album[0].id);
                        if (index !== -1) {
                          carts[index].quantite += 1;
                          carts[index].total += carts[index].quantite * carts[index].prix;
                        } else {
                          carts.push(
                            new ShoppingCart(
                              album[0].id,
                              1,
                              album[0].prix,
                              album[0].titre,
                              album[0].prix
                            )
                          );
                        }
                      } else {
                        carts.push(
                          new ShoppingCart(
                            album[0].id,
                            1,
                            album[0].prix,
                            album[0].titre,
                            album[0].prix
                          )
                        );
                      }
                      this.localstorage.setItem("cart", carts);
                      let showCarts = [...this.state.carts];
                      showCarts = carts;
                      this.setState(prevState => { 
                                                   return { carts: showCarts }
                                                 });
                    }
                  });
  }
  render() {
    const { chargement, erreur, albums, carts } = this.state;
    if (chargement) {
      return <p>Chargement ...</p>;
    }
    if (erreur) {
      return (
        <p>
          Désolé! Une erreur s'est produite lors du chargement des données.&nbsp;
          <button onClick={this.loadData}>Try again</button>
        </p>
      );
    }
    let id = this.props.match.params.id;
    return (
      <React.Fragment>
          <MSHeader myCart={ carts } title={ 'BOUTIQUE DE MUSIC' } checkoutVal={ 'Checkout' } />
          <div className="hpanel">
            <div className="panel-body">
                <ul id="ms-album-list">
                    {  albums.map((value, key) => value.genre_id === Number(id) ? <MSShowAlbum alKey ={ key } alValue = { value } onSubmit={ this.AddItem } />: "") }
                </ul>
            </div>
          </div>
      </React.Fragment>
    );
  }
}
