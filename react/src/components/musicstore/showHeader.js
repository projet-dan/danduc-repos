import React from "react";

const ShowHeader = (props) => {
    return (
        <thead>
            <tr>
                <th style={props.thStyle}>Description</th>
                <th style={props.thStyle}>Quantité</th>
                <th style={props.thStyle}>Prix</th>
                <th style={props.thStyle}>Total</th>
            </tr>
        </thead>
    );
}

export default ShowHeader;