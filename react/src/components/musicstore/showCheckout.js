import React from "react";
import CurrencyFormat from "react-currency-format";
import { Link } from "react-router-dom";
import ShowHeader from './showHeader';
import ShowTotal from './showTotal';

const ShowCheckout = (props) =>  {
    const removeItem = (event,albumID) => {
        event.preventDefault();
        props.onRemoveItem(albumID);
    }
    const onChangeInput = (event) => {
        event.preventDefault();
        props.onInput(event.target.id, event.target.value)
    }
    const updateItem = (event) => {
        event.preventDefault();
        props.onUpdateItem();
    }
    return (
        <table className="table table-bordered" role="grid">
            <ShowHeader thStyle={ props.thStyle } />
            <tbody>
                { props.carts.map((value, key) =>
                    (
                    <tr key={key}>
                        <td>{value.nom}</td>
                        <td style={{ textAlign: "right", width: "10%" }}>
                        <input 
                               type="text"
                                {...{ id: value.id }}
                                size="4"
                                defaultValue={value.quantite}
                                onBlur={ onChangeInput } 
                               />
                        </td>
                        <td style={{ textAlign: "right", width: "10%" }}>
                            <CurrencyFormat value={ value.prix.toFixed(2)}
                                                    displayType={"text"}
                                                    thousandSeparator={true}
                                                    prefix={"$"}/>
                        </td>
                        <td style={{ textAlign: "right", width: "15%" }}>
                            <CurrencyFormat value={ value.total.toFixed(2)}
                                                    displayType={"text"}
                                                    thousandSeparator={true}
                                                    prefix={"$"}/>
                        </td>
                        <td style={{ width: "22%" }}>
                            <button type="submit" className="btn btn-success" onClick={ () => updateItem }>Modifier</button>&nbsp;&nbsp;
                            <Link to="#"
                                  className="btn btn-danger"
                                  onClick={ event => {
                                                if (
                                                    window.confirm(
                                                    "Êtes-vous sûre de vouloir supprimer cet item?"
                                                    )
                                                )
                                                removeItem(value.id);
                                                else event.preventDefault();
                                                }
                                            }>Supprimer</Link>
                        </td>
                    </tr>
                    ))}
                    <ShowTotal carts={ props.carts } taxes={ props.taxes } />
            </tbody>
        </table>
    );
}

export default ShowCheckout;