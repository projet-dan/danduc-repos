/*
* Program : Show List Component
* Écrit par : Dan Duc Dao
*/

import React from "react";
import CurrencyFormat from "react-currency-format";
import { NavLink } from "react-router-dom";

const MSShowAlbum = props => {
     const AddItem = (event,albumID) => {
         event.preventDefault();
         props.onSubmit(albumID);
     }
     return (
        <li key={ props.alKey }>
            <div>
                <p><NavLink to="#"><img alt={ props.alValue.titre } src={ props.alValue.photo} /></NavLink></p>
                <p>
                    <button type="submit"
                            name="ok"
                            className="btn btn-success"
                            onClick={ event => AddItem(event, props.alValue.id) }>
                        <span style={{ fontWeight: "bold",
                                    padding: 3,
                                    fontSize: 12,
                                    color: "#fff"
                                    }}>Ajouter</span>
                    </button>
                </p>
            </div>
            <div>
                <div>{ props.alValue.titre }</div>
                <div>
                    Prix : <CurrencyFormat value={ props.alValue.prix }
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"$"}/>
                </div>
            </div> 
        </li>
     );
}

export default MSShowAlbum; 