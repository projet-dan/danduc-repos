import React from "react";
import CurrencyFormat from "react-currency-format";

const ShowTotal = (props) => {
    const calcTotal = () => {
        var total = 0;
        var len = props.carts.length;
        for (var i = 0; i < len; i++) {
          total += parseFloat(props.carts[i].total);
        }
        return total;
    }
    const Total = () => {
        return calcTotal();
    }
    const TaxeTPS = () => {
        return parseFloat((calcTotal() * props.taxes.TPS) / 100).toFixed(2);
    }
    const TaxeTVQ = () => {
        return parseFloat((calcTotal() * props.taxes.TVQ) / 100).toFixed(2);
    }
    const GrandeTotal = () => {
        return (
          parseFloat(calcTotal()) +
          parseFloat(TaxeTPS()) +
          parseFloat(TaxeTVQ())
        );
    }
    return (
        <tr>
            <td colSpan="3" />
            <td>
                <table width="100%">
                    <tbody>
                        <tr>
                            <td><strong>Total:</strong></td>
                            <td style={{ textAlign: "right" }}>
                                <CurrencyFormat value={Total().toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"} />
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TPS({ props.taxes.TPS }%):</strong></td>
                            <td style={{ textAlign: "right" }}>
                                <CurrencyFormat value={TaxeTPS()}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TVQ({ props.taxes.TVQ }%):</strong></td>
                            <td style={{ textAlign: "right" }}>
                                <CurrencyFormat value={TaxeTVQ()}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Grande Total:</strong></td>
                            <td style={{ textAlign: "right" }}>
                                <CurrencyFormat value={GrandeTotal().toFixed(2)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"$"}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    );
}

export default ShowTotal;