/*
* Program : Checkout component
* Écrit par : Dan Duc Dao
*/

import React, { Component } from "react";
import { LocalStorage } from "../../classes/localstorage";
import MSHeader from "./header";
import ShowCheckout from './showCheckout';

export default class Checkout extends Component {
  constructor() {
    super();
    this.localstorage = new LocalStorage();
    this.state = {
      thStyle: { textAlign: "center" },
      carts: [],
      taxes : {
               TPS: 9.15,
               TVQ: 7.13
              }
    };
  }
  removeItem = (cartId) => {
    if (this.localstorage.itemExist("cart")) {
      let carts = this.localstorage.getItem("cart")
                                   .filter(value => value.id !== parseInt(cartId));
      this.localstorage.setItem("cart", carts);
      this.setState(prevState => {
           return { carts: carts }
      });
      if (carts.length === 0) 
         this.localstorage.removeItem("cart");
    }
  }
  updateCart = () => {
     //TODO Savegarder dans bd.
  }
  removeItem = (albumId) => {
      //TODO Savegarder dans bd.
  }
  saveInput = (albumId,quantite) => {
      if (quantite === "") {
        alert("La quantité est obligatoire");
        return;
      }
      if (!quantite.match(/^[1-9][0-9]*$/)) {
        alert("La quantité doit être digit et ne contient pas de zéro");
        return;
      }
      let carts = this.localstorage.getItem("cart")
                                   .map(item => {
                                       if(item.id === parseInt(albumId))
                                       {
                                           item.quantite = parseInt(quantite)
                                           item.total = item.prix * item.quantite;
                                       }
                                       return item;
                                    }); 
                    
      this.localstorage.setItem("cart", carts);
  }
  componentDidMount() {
    if (this.localstorage.itemExist("cart")) {
      this.setState(prevState => { 
                        return { carts: this.localstorage.getItem("cart") }
                    });
    }
  }

  render() {
    const { carts, thStyle, taxes} = this.state;
      return (
        <React.Fragment>
          <MSHeader myCart={ carts } title={ 'BOUTIQUE DE MUSIC' } checkoutVal={ 'Checkout' } />
          <div className="hpanel">
              <div className="panel-body">
                  <ShowCheckout thStyle={ thStyle } 
                                taxes={ taxes }
                                carts={ carts } 
                                onInput={ this.saveInput }
                                onUpdateItem={ this.updateCart }
                                onRemoveItem={ this.removeItem }
                                />
              </div>
          </div>
        </React.Fragment>
      );
  }
}
