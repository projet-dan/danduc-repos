import React, { Component }  from "react";
import SeasonDisplay from './SeasonDisplay';
import Spinner from '../../spinner';

export default class Season extends Component{
    constructor(props){
        super(props);
        this.state = {
            lat:null,
            errorMessage:""
        };
        window.navigator.geolocation.getCurrentPosition( 
            (position) => {
                this.setState(prevState =>{ 
                   return { lat:position.coords.latitude }
                })
            },
            (err) => {
                this.setState(prevState =>{ 
                    return { errorMessage:err.message }
                 })
            }
        );
    }
    renderContent = () => {
        if(this.state.errorMessage && !this.state.lat)
        {
            return <div>Error:{ this.state.errorMessage }</div>  
        }
       
        if(!this.state.errorMessage && this.state.lat)
        {
            return <div><SeasonDisplay lat={ this.state.lat } /></div>   
        }

        return <div><Spinner message="Veuillez accepter la location" /></div>
    }
    render(){
       return ( 
            <div className="hpanel">
                <div className="panel-body">
                   <div>{ this.renderContent() }</div> 
                </div>
            </div>
        )
    }
};

