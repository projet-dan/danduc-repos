/*
* Program : Blog component
* Écrit par : Dan Duc Dao
*/

import React from "react";
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import PostList from './PostList';
import faker from 'faker';

const Blog = () => {
  return (
    <div className="content">
      <div className="row">
        <div className="hpanel hblue col-md-6">
            <div className="panel-heading hbuilt">Blog Post</div>
            <div className="panel-body"> 
                <ApprovalCard>
                    <CommentDetail author="Sam" 
                               timeAgo="Today at 4:45PM" 
                               content="Nice blog post" 
                               avatar={ faker.image.avatar() } /> 
                 </ApprovalCard>
                 <ApprovalCard>
                   <CommentDetail author="Alex" 
                               timeAgo="Today at 2:00PM" 
                               content="I like the subject" 
                               avatar={ faker.image.avatar() } />
                </ApprovalCard>
                <ApprovalCard>
                    <CommentDetail author="Jane" 
                               timeAgo="Yesterday at 5:00PM" 
                               content="I like the writing" 
                               avatar={ faker.image.avatar() } />
                </ApprovalCard>
            </div>
        </div>
      </div>  
      <div className="row">
        <div className="hpanel hblue col-md-6">
            <div className="panel-heading hbuilt">Post List</div>
            <div className="panel-body"> 
                <PostList />
            </div>
        </div>
      </div>                   
    </div>
  );
}

export default Blog;
