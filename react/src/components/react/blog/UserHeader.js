import React from 'react';
import { connect } from 'react-redux';
import  { fetchUsers }  from '../../../actions/blog/blog';

class UserHeader extends React.Component {
    componentDidMount(){
        this.props.fetchUsers();
    }
    renderUser = (userId,users) => {
        let user = users.find(item => item.id === userId);
        if(user)
        {
            return (
                <div>{ user.name }  </div>
           )
        }  
    }
    render(){
        return (
            <div>{ this.renderUser(this.props.userId, this.props.users) }</div>
        );
    }
}

const mapStateToProps = state => {
     return {
         users:state.users
     }
}

export default connect(mapStateToProps,{ fetchUsers })(UserHeader);