import React from 'react';
import { NavLink } from "react-router-dom";

const ApprovalCard = props => {
    return (
      <div className="card approved-card">
         <ul className="list-group list-group-flush">
            <li className="list-group-item"> Are you sure?</li>
            <li className="list-group-item">{ props.children }</li>
            <li className="list-group-item">
                <NavLink to="#" className="btn btn-success">Approve</NavLink>&nbsp;
                <NavLink to="#" className="btn btn-danger">Reject</NavLink>
            </li>
         </ul>
      </div>
     );
}

export default ApprovalCard;