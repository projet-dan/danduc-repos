import React from "react";

 const CommentDetail = props => {
     return(
            <div className="blog">
                <div>
                <a href="/">
                    <img alt="avatar" src={ props.avatar } />
                </a>
                </div>
                <div>
                <div>
                    <a href="/"><strong>{ props.author }</strong></a>
                    </div>
                    <div>
                    <span>{ props.timeAgo }</span>
                    </div>
                    <div>{ props.content }</div>
                </div>
            </div>        
            );
}

export default CommentDetail;