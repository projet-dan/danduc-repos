import React from 'react';
import { connect } from 'react-redux';
import  { fetchPosts }  from '../../../actions/blog/blog';
import UserHeader from './UserHeader';

class PostList extends React.Component {
    componentDidMount() {
        this.props.fetchPosts();
    }
    renderList = (posts) => {
        return posts.map(item => {
            return (
                <div key={ item.id } className="posts">
                    <div className="box-1">
                        <i className="fa fa-user"></i>
                    </div>
                    <div className="box-2">
                        <h3>{ item.title } </h3>
                        <div>{ item.body }</div> 
                        <UserHeader userId={ item.userId } />
                    </div>
                   
                </div>
                   
            );
        });
    }
    render(){
         return <div>{ this.renderList(this.props.posts) }</div>
     }
}

const mapStateToProps = (state) => {
    return {
        posts:state.posts
    }
}

export default connect(mapStateToProps,{ fetchPosts })(PostList);