import React, { Component } from "react";
import { connect } from 'react-redux';
import  { selectedItem }  from '../../../actions/searchBar/searchBar';

class SelOptions extends Component {
    render(){
        let { currentItemState, currentItem} = this.props;
        const myCurrentItem = currentItemState != null? currentItemState : currentItem;
        return myCurrentItem.map(item => {
            return (
                    <div key={ item.id }>
                        <div className={ item.clsAttribut } style={{ position: 'relative' }} onClick={ () => this.props.selectedItem(myCurrentItem,item) }>
                            <input type="checkbox" name={ item.name } value={ item.value } />
                            <ins className="iCheck-helper" />
                        </div>&nbsp;{ item.text }&nbsp;
                    </div>
                );
        });
    }
}

const mapStateToProps = (state) => {
    return {
        currentItemState : state.currentItemState
    };
};

export default connect(mapStateToProps,{ selectedItem })(SelOptions);