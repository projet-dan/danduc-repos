import React from "react";
import { connect } from 'react-redux';

const SearchList = (props) => {
    const renderCollectionList = () => {
        const { currentItemSelected } = { ...props };
        if(currentItemSelected)
        {
            return currentItemSelected.map(element => {
                    return (
                        <tr>
                            <td>{ element.title }</td>
                            <td>{ element.description }</td>
                            <td>{ element.total_photos }</td>
                        </tr>
                    );
                });
            }
    }
    return (
            <div className="hpanel hblue">
                <div className="panel-heading hbuilt">List of collections</div>
                <div className="panel-body"> 
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Number of photo</th>
                                </tr>
                            </thead>
                            <tbody> { renderCollectionList() }</tbody>
                        </table>
                    </div>
                </div>
            </div>   
        );
}

const mapStateToProps = (state) => {
    return {
        currentItemSelected : state.currentItemSelected 
    };
};

export default connect(mapStateToProps)(SearchList);