import React, { Component } from "react";
import { connect } from 'react-redux';
import SelOptions from './SelOptions';
import SearchTerm from './SearchTerm';
import SearchList from './SearchList';
import { fetchCollections } from '../../../actions/searchBar/collection';

class searchBar extends Component {
    componentDidMount(){
        this.props.fetchCollections();  
    }
    render(){
        return (
            <form className="form-horizontal">
                <div className="content searchBar">
                    <div className="row">
                        <div className="hpanel hblue col-md-6">
                            <div className="panel-heading hbuilt">Collection</div>
                            <div className="panel-body"> 
                                <SearchTerm />
                               <div className="form-group">
                                    <div className="checkbox">
                                        <SelOptions currentItem={ this.props.initItem } />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <SearchList />
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        initItem : state.initItem 
    };
};

export default connect(mapStateToProps, { fetchCollections })(searchBar);