import React, {Component} from "react";
import { connect } from 'react-redux';
import  { doSearchInList }  from '../../../actions/searchBar/searchBar';

class SearchTerm extends Component {
     render(){ 
         let { collections, currentItemState } = { ...this.props };
         return (
            <div className="form-group search-term">
                <label className="control-label col-sm-2" htmlFor="search" style={{ paddingLeft:"0px" }}>Search</label>
                <div className="col-sm-8">
                    <input type="search" 
                           className="form-control" 
                           id="search" 
                           onChange={ (event) => this.props.doSearchInList(event.target.value,
                                                                           currentItemState,
                                                                           collections) } />
                </div>
            </div>
         );
     }
}

const mapStateToProps = (state) => {
    return {
        currentItemState : state.currentItemState,
        collections : state.collections
    };
};

export default connect(mapStateToProps,{ doSearchInList })(SearchTerm);