import React from 'react';
import SongList from './SongList';
import SongDetail from './SongDetail';

const Songs = () => {
    return (
        <div className="row">
            <div className="hpanel hblue col-sm-6">
                <div className="panel-heading hbuilt">Song List</div>
                <div className="panel-body"> 
                   <div className="row">
                       <div className="col-sm-6">
                            <SongList />
                        </div>
                        <div className="col-sm-6">
                            <SongDetail />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    );
}

export default Songs;





