import React from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../../../actions/song/song';

class SongList extends React.Component {
    renderList = () =>{
        return this.props.songs.map((song) => {
             return (
                <div className="row" key={ song.title } style={ { borderBottom:'1px solid #ccc', margin:"0 0 5px 0",padding:"0 0 5px 0" }}>
                    <div className="col-sm-9">{ song.title }</div>
                    <div className="col-sm-3">
                        <button className="btn btn-success" onClick={ () => this.props.selectSong(song)}>Select</button>
                    </div>
                </div>
             );
        });
    }
    render(){
        return (
            <div>{ this.renderList() }</div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        songs:state.songs
    }

}

export default connect(mapStateToProps,{ selectSong })(SongList);