import React from 'react';
import { connect } from 'react-redux';

const SongDetail = ( { mySelectedSong }) => {
    if(!mySelectedSong)
    {
        return <div>Select a song</div> 
    }
    return (
        <React.Fragment>
            <h3>Detail for :</h3>
            <p>
                <strong>Title : { mySelectedSong.title }</strong>
            </p>
            <p>
                <strong>Duration : { mySelectedSong.duration }</strong>
            </p>
        </React.Fragment>
    )
}

const mapStateToProps = (state) => {
    return { mySelectedSong: state.selectedSong }
}

export default connect(mapStateToProps)(SongDetail);