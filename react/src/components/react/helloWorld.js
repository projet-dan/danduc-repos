import React from 'react';

const HelloWorld = () => {
    return (
             <div className="content">
                <div className="row">
                    <div className="hpanel hblue col-md-12">
                        <div className="panel-body">Hello world</div>
                    </div>
                </div>
             </div> 
            )        
};

export default HelloWorld;