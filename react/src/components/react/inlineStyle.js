import React from 'react';

const InlineStyle = () => {
    return (
            <div className="content">
                <div className="row">
                    <div className="hpanel hblue col-md-12">
                        <div className="panel-body"> 
                            <label>Enter your name : </label>
                            <input id="name" type="text" />
                            <button style={{ backgroundColor:'blue', color:'white'}}>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        )
};

export default InlineStyle;