import React from 'react';

function getTime(){
    return (new Date()).toLocaleDateString();
}

const GetTime = () => {
    return (
             <div className="content">
                <div className="row">
                    <div className="hpanel hblue col-md-12">
                        <div className="panel-body">
                            <div>Current Time : </div>
                            <h3>{ getTime() }</h3>
                        </div>
                    </div>
                </div>
             </div> 
            )        
};

export default GetTime;