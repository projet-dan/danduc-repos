import { Service } from "../services/service";

const service = new Service();
service.setProtocol("https://");
service.setDomain("api.unsplash.com");
service.setAuthorization('Client-ID a7cd0e3356a5dcf1574ae7d63f94e87d0f131650365f897c4d7a4f8fae2ae8ae');

export default service;