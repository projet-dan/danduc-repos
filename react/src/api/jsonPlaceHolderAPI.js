import { Service } from "../services/service";

const service = new Service();
service.setProtocol("https://");
service.setDomain("jsonplaceholder.typicode.com");

export default service;