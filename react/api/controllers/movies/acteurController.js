"use strict";

const { OK, BAB_REQUEST } = require('../../config');

module.exports = function(app, movie) {
  app.put("/movieadmin/acteur/:id", function(req, res, next) {
    movie.query("UPDATE acteurs " +
                "INNER JOIN acteur_films ON acteurs.id = acteur_films.acteur_id " +
                "SET acteurs.active = ?, acteur_films.active = ? " +
                "WHERE acteurs.id = ?",
                [0, 0, req.params.id],
                function(error, result) 
                {
                  if (error || !result || result.affectedRows === 0)
                  {
                      if(error)
                        console.error(`${error.code}\n${error.sqlMessage}`);
                      else
                        console.error(`row(s) updated : ${result.affectedRows}`);
                      return res.status(BAB_REQUEST).send();
                  }
                  return res.status(OK).send({ success: true });
                });
    });
};
