"use strict";

const { OK, BAB_REQUEST } = require('../../config');

module.exports = function(app, food) {
  app.get("/fournisseur", function(req, res, next) {
    food.query("SELECT * FROM fournisseurs ORDER BY nom", function(error,results) {
      if (error)
      {
         console.error(`${error.code}\n${error.sqlMessage}`);
         return res.status(BAB_REQUEST).send();
      }
      return res.status(OK).send(results);
    });
  });

  app.get("/fournisseur/:id", function(req, res, next) {
    const id = req.params.id;
    if (!id || id === undefined) 
       return res.status(BAB_REQUEST).send();
    food.query("SELECT * FROM fournisseurs WHERE id = ?", id, function(error,result) {
      if (error) 
      {
         console.error(`${error.code}\n${error.sqlMessage}`);
         return res.status(BAB_REQUEST).send();
      }
      return res.status(OK).send(result[0]);
    });
  });
};
