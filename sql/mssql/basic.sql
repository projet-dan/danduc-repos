use Northwind;

SELECT  FirstName, LastName 
FROM dbo.Employees;

SELECT * FROM dbo.Employees;

--Concatening
SELECT LastName + ', ' + FirstName
FROM dbo.Employees;

--Aliasing column names
SELECT LastName + ', ' + FirstName AS [Full Name]
FROM dbo.Employees;

SELECT FullName = LastName + ', ' + LastName
FROM dbo.Employees;

SELECT DISTINCT Title FROM dbo.Employees;

SELECT CompanyName, City
FROM dbo.Customers
WHERE City = 'Paris';

SELECT CompanyName
FROM dbo.Customers
WHERE CompanyName LIKE 'S%';

SELECT CompanyName
FROM dbo.Customers
WHERE CompanyName LIKE '%S';

SELECT CompanyName
FROM dbo.Customers
WHERE CompanyName LIKE '%S%';

SELECT CustomerID
FROM dbo.Customers
WHERE CustomerID LIKE 'B___P';

SELECT CustomerID
FROM dbo.Customers
WHERE CustomerID LIKE 'FRAN[RK]';

SELECT CustomerID
FROM dbo.Customers
WHERE CustomerID LIKE 'FRAN[A-S]';

SELECT CustomerID
FROM dbo.Customers
WHERE CustomerID LIKE 'FRAN[^R]';

SELECT LastName, FirstName, PostalCode
FROM dbo.Employees
WHERE PostalCode BETWEEN '98103' AND '98999';

--Testing for null
SELECT LastName, FirstName, Region
FROM dbo.Employees
WHERE Region IS NULL;

--Use AND condition
SELECT LastName,City,PostalCode
FROM dbo.Employees
WHERE City = 'Seattle' AND PostalCode LIKE '9%';

--Use OR condition
SELECT LastName,City,PostalCode
FROM dbo.Employees
WHERE City = 'Seattle' OR PostalCode LIKE '9%';

SELECT LastName,City,PostalCode
FROM dbo.Employees
WHERE City NOT LIKE 'Seattle';

--Operator precedetn : NOT, AND, OR
SELECT LastName,City,PostalCode
FROM dbo.Employees
WHERE LastName LIKE '%S%'
AND City NOT LIKE 'Seattle';

--IN
SELECT CustomerID, Country
FROM dbo.Customers
WHERE Country IN ('France', 'Spain');

--IN with a subquery
SELECT CustomerID
FROM dbo.Customers
WHERE CustomerID NOT IN (SELECT CustomerID 
                         FROM dbo.Orders);

--ORDER BY
SELECT LastName, City
FROM dbo.Employees
ORDER BY City;

--Sorting in descending order
SELECT LastName, City
FROM dbo.Employees
ORDER BY City DESC;

--Sorting multiple columns
SELECT LastName, City
FROM dbo.Employees
ORDER BY City DESC, LastName ASC;

--Sorting on an expression
SELECT LastName
FROM dbo.Employees
ORDER BY LEN(LastName);

--Agregate function : COUNT, COUNT_BIG, SUM, AVG, MIN, MAX
SELECT COUNT(*)
FROM dbo.Employees;

SELECT COUNT(*) AS NumEmployees, -- with null value
COUNT(Region) AS NumRegion -- with no null value
FROM dbo.Employees;

--Counting with where
SELECT COUNT(*) AS NumEmployeeSeattle
FROM dbo.Employees
WHERE City = 'Seattle';

--Using grouping
SELECT City, COUNT(*) AS NumEmployees
FROM dbo.Employees
GROUP BY City;

--ORDER BY with GROUP BY
SELECT City, COUNT(*) AS NumEmployees
FROM dbo.Employees
GROUP BY City
ORDER BY COUNT(*) DESC, City;

--HAVING
SELECT City, COUNT(*) as NumEmployees
FROM dbo.Employees
GROUP BY City
HAVING COUNT(*) > 1
ORDER BY NumEmployees DESC, City;

SELECT TOP 3 City, COUNT(*) AS NumEmployees
FROM dbo.Employees
GROUP BY City
ORDER BY COUNT(*) DESC;

SELECT TOP 3 WITH TIES City, COUNT(*) AS NumEmployees
FROM dbo.Employees
GROUP BY City
ORDER BY COUNT(*) DESC;

SELECT TOP 25 PERCENT WITH TIES City, COUNT(*) AS NumEmployees
FROM dbo.Employees
GROUP BY City
ORDER BY COUNT(*) DESC;