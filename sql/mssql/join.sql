use Northwind;

--Cartesian 
SELECT ProductName, CategoryName
FROM dbo.Products, dbo.Categories;

SELECT ProductName, CategoryName
FROM dbo.Products CROSS JOIN dbo.Categories;

SELECT ProductName, CategoryName
FROM dbo.Products, dbo.Categories
WHERE dbo.Products.CategoryID = dbo.Categories.CategoryID;

SELECT dbo.Products.ProductName, dbo.Categories.CategoryName
FROM dbo.Products JOIN dbo.Categories
ON dbo.Products.CategoryID = dbo.Categories.CategoryID;

--Using join
SELECT dbo.Products.ProductName, dbo.Categories.CategoryName
FROM dbo.Products JOIN dbo.Categories
ON dbo.Products.CategoryID = dbo.Categor/ies.CategoryID;

--with table aliases
SELECT P.ProductName, C.CategoryName
FROM dbo.Products AS P JOIN dbo.Categories AS C
ON P.CategoryID = C.CategoryID;

--Using INNER JOIN
SELECT P.ProductName, C.CategoryName
FROM dbo.Products AS P 
INNER JOIN dbo.Categories AS C
ON P.CategoryID = C.CategoryID;

--JOIN with Where and Order By
SELECT P.ProductName, C.CategoryName, P.UnitPrice
FROM dbo.Products AS P 
INNER JOIN dbo.Categories AS C
ON P.CategoryID = C.CategoryID
WHERE P.UnitPrice > 50
ORDER BY P.ProductName;

--Multiple table join
SELECT OrderID, 
       convert(varchar(10),OrderDate,101) AS Date, 
       CompanyName,
       LastName
FROM Orders 
INNER JOIN Customers
ON Orders.CustomerID = Customers.CustomerID
INNER JOIN dbo.Employees
ON Orders.EmployeeID =  Employees.EmployeeID
WHERE OrderDate BETWEEN '9/1/1996' AND '9/10/1996'
ORDER BY OrderDate;

--JOins and Aggregates
SELECT CompanyName,
       SUM([Order Details].UnitPrice * [Order Details].Quantity) AS TotalSold
FROM Customers
INNER JOIN Orders   
ON Customers.CustomerID = Orders.CustomerID 
INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
WHERE Orders.OrderDate BETWEEN '9/1/1996' AND '9/10/1996'
GROUP BY CompanyName
ORDER BY TotalSold DESC;

SELECT CompanyName, MIN(Orders.OrderDate) AS FirstOrder
FROM dbo.Customers 
INNER JOIN dbo.Orders
ON Customers.CustomerID = Orders.CustomerID
GROUP BY CompanyName
ORDER BY CompanyName

SELECT CompanyName, MIN(Orders.OrderDate) AS FirstOrder
FROM dbo.Customers 
LEFT JOIN dbo.Orders
ON Customers.CustomerID = Orders.CustomerID
GROUP BY CompanyName
ORDER BY CompanyName

--Left outer join with unmatched values
SELECT CompanyName AS [No Order]
FROM dbo.Customers 
LEFT JOIN dbo.Orders
ON Customers.CustomerID = Orders.CustomerID
WHERE Orders.OrderID IS NULL
ORDER BY CompanyName;

--Rigth join with unmatched values
SELECT CompanyName AS [No Order]
FROM dbo.Orders 
RIGHT JOIN dbo.Customers
ON Customers.CustomerID = Orders.CustomerID
WHERE Orders.OrderID IS NULL
ORDER BY CompanyName;

--Full joins for diplay nulls on both side of the join
SELECT ProductName, CategoryName
FROM dbo.Products FULL JOIN dbo.Categories
ON dbo.Products.CategoryID = dbo.Categories.CategoryID
ORDER BY ProductName;

--Inner joins first, then outer joins
SELECT CompanyName,
       SUM([Order Details].UnitPrice * [Order Details].Quantity) AS TotalSold
FROM dbo.Orders 
INNER JOIN dbo.[Order Details]
ON Orders.OrderID = [Order Details].OrderID
RIGHT JOIN dbo.Customers
ON Orders.CustomerID = Customers.CustomerID
GROUP BY CompanyName
ORDER BY CompanyName;

--Two left joins
SELECT CompanyName,
       SUM([Order Details].UnitPrice * [Order Details].Quantity) AS TotalSold
FROM dbo.Customers
LEFT JOIN dbo.Orders
ON Orders.CustomerID = Customers.CustomerID
LEFT JOIN dbo.[Order Details]
ON Orders.OrderID = dbo.[Order Details].OrderID
GROUP BY CompanyName
ORDER BY CompanyName;

--Self join to list all employees and managers
SELECT Employees.FirstName + ' ' + Employees.LastName AS EmployeeName,
       Managers.FirstName + ' ' + Managers.LastName AS ManagerName
FROM dbo.Employees 
INNER JOIN dbo.Employees AS Managers
ON Employees.ReportsTo = Managers.EmployeeID

--Self join to list all employees
SELECT Employees.FirstName + ' ' + Employees.LastName AS EmployeeName,
       Managers.FirstName + ' ' + Managers.LastName AS ManagerName
FROM dbo.Employees 
LEFT JOIN dbo.Employees AS Managers
ON Employees.ReportsTo = Managers.EmployeeID