use Northwind;

INSERT INTO dbo.Categories (CategoryName)
VALUES('Vitamins');

--Suppress the return of rows affected
SET NOCOUNT ON;
INSERT INTO dbo.Categories(CategoryName)
VALUES('Vitamin');

--Retrieve the new identity column name
SELECT @@IDENTITY AS NewCategoryID;
SELECT SCOPE_IDENTITY() AS NewCategoryID;
SELECT IDENT_CURRENT('dbo.Categories') AS NewCategoryID;
